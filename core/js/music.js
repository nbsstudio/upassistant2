/**
 * 点歌系统
 * @author 周毅豪
 * @license GPL v2.0
 */
var fs = require("fs");
var path = require("path");
var http = require("http");
var url = require("url");
var execSync = require("child_process").execSync;
var id3_reader = require("id3_reader");
// let api = parent.parent.window.API;
let api = nw.global.CoreAPI;
var layer = require('layer-src');
let smfl = 0; 

module.exports = {
    /**
     * 本地音乐服务，用于网页加载
     * @param {*} frame 
     * @param {*} errorEvent 
     */
    musicService(frame, errorEvent) {
        http.createServer(function (request, response) {
            try {
                const INCOMING_URL = decodeURIComponent(request.url);
                let path = './cache' + INCOMING_URL;
                const fileType = path.substr(path.lastIndexOf('.'));
                //遇到M4A就转换格式
                //调用ffmpeg：ffmpeg -i .\a.m4a -f mp3 a.mp3
                if (path.indexOf('.m4a') > 0) {
                    let mp3Path = path + '.mp3';
                    if (!fs.existsSync(mp3Path)) {
                        //自动判断系统并调用相应的命令
                        let osv = frame.getOSVersion();
                        let ffmFlag = osv.name == 'Windows' ? 'ffmpeg.exe' : 'ffmpeg';
                        if (osv.name == 'Windows') {
                            path = path.replace(/\//g, '\\');
                            mp3Path = mp3Path.replace(/\//g, '\\');
                        }
                        let cmdStr = ffmFlag + ' -i ' + path + ' -f mp3 ' + mp3Path;
                        execSync(cmdStr);
                    }
                    path = mp3Path;// 音频转换成功，替换文件路径，相当于所有关于m4a文件的请求全部返回成mp3文件的内容
                }

                //根据URL找回对应的音频文件
                if (fs.existsSync(path)) {
                    let f = fs.createReadStream(path);
                    response.writeHead(200, {
                        'Content-Type': 'video/quicktime;charset=UTF-8',
                        'Content-Disposition': 'inline; filename=a' + fileType
                    });
                    f.pipe(response);
                } else {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.end(JSON.stringify({ code: 404, message: 'File Not Exists!' }));
                }

            } catch (e) {
                response.writeHead(200, { 'Content-Type': 'text/plain' });
                response.end(JSON.stringify({ code: 500, message: e.message }));
                errorEvent({ url: decodeURIComponent(request.url), error: e });
            }

        }).listen(35);
    },
    searchMusic(keyword, callback) {
        //搜索本地音乐
        let localMusic = this.searchLocalMusic(keyword);
        let localMusicValues = [];
        if(localMusic && localMusic.length > 0){localMusicValues = localMusic[0].values};
        //搜索在线音乐
        $.get('http://nacgame.com:8888/netease/songs/' + keyword + '/search/0/10', function (response) {
            try {
                let datajson = JSON.parse(response);
                datajson.code = 200;
                patchResult(localMusicValues, datajson);
                callback(datajson);
            } catch (error) {
                if(localMusicValues.length>0){
                    let datajson = {code:200,data:{count:localMusicValues.length,data:[]}};
                    patchResult(localMusicValues, datajson);
                    callback(datajson);
                }else{
                    callback({ code: 500, message: error.message });
                }
            }
        });
    },
    /**
     * 从已缓存的本地歌库列表中找音乐
     * @param {string} keyword 
     * @returns 
     */
    searchLocalMusic(keyword) {
        let data = api.getDB() ? api.getDB().exec("select artist,title,path,lib_path from local_music_lib where title like '%"+keyword+"%' or artist like '%"+keyword+"%'") : [];
        return data;
    },
    getUrl(id, callback) {
        $.get('http://nacgame.com:8888/netease/song/' + id + '/url', function (response) {
            try {
                let datajson = JSON.parse(response);
                callback(datajson);
            } catch (error) {
                console.log(error);
            }
        });
    },
    getMusic(musicObj, callback) {
        let testPath = './cache/' + musicObj.artist + '_' + musicObj.name.replace('/', '-!-').replace(' ', '_') + '_rd';
        this.createCacheDirIfNotExists();
        cleanFile('./cache');
        //区分本地和网络的音乐加载
        if(musicObj.hasOwnProperty("local") && musicObj.local){
            //根据文件命名规则把本地音乐复制到缓存目录
            const fileType = musicObj.path.substr(musicObj.path.lastIndexOf('.'));
            testPath += fileType;
            fs.copyFileSync(musicObj.path,testPath);
            callback({ code: 200, data: testPath });
        }else{
            this.getUrl(musicObj.id, function (data) {
                let URL = data.data.url;
                if (URL != null) {
                    const URL_P = url.parse(URL);
                    const options = {
                        host: URL_P.hostname,
                        port: URL_P.port,
                        path: URL_P.path
                    };
                    const fileType = URL_P.path.substr(URL_P.path.lastIndexOf('.'));
                    testPath += fileType;
                    if (fs.existsSync(testPath)) {
                        //本地已存在，已经点播过的，可以不用下载
                        callback({ code: 200, data: testPath });
                    } else {
                        // 模拟客户端访问，下载音频文件
                        const req = http.request(options, function (res) {
                            //数据块接收时:
                            res.on('data', function (chunk) {
                                let buff = Buffer.from(chunk, 'binary');
                                //数据块接收时，由于不是一次性接收完毕的，每接收一次报文就追加当前文件
                                if (fs.existsSync(testPath)) {
                                    fs.appendFileSync(testPath, buff);
                                } else {
                                    fs.writeFileSync(testPath, buff);
                                }
                            });
                            //接收结束时:
                            res.on("end", function () {
                                callback({ code: 200, data: testPath });
                            });
                        });
                        //接收过程出错时：
                        req.on('error', function (e) {
                            console.log('problem with request: ' + e.message);
                            callback({ code: 500, data: e, message: e.message });
                        });
                        req.end();
                    }
                } else {
                    callback({ code: 500, data: null, message: '无版权，无法下载！' });
                }
            });
        }
    },
    createCacheDirIfNotExists() {
        if (!fs.existsSync('./cache/')) {
            fs.mkdirSync('./cache/');
        }
    },
    /**
     * 扫描本地音乐库
     * @param {array}} localLibs 
     */
    scanLocalMusic(localLibs) {
        if (localLibs && localLibs.length > 0) {
            for (const key in localLibs) {
                if (Object.hasOwnProperty.call(localLibs, key)) {
                    const element = localLibs[key];
                    if (!element.disable) {
                        //扫描本地目录
                        scanMusicFile(element.path, element.path);
                    }
                }
            }
        }
        //提供搜索索引
    },
    /**
     * 是否正在扫描本地音乐目录
     * @returns 
     */
    isScanningLocalMusic(){
        return smfl > 0;
    }
}


/*
 * 内部方法
 */
//补充本地库中的音乐搜索资料
function patchResult(localMusicValues, datajson) {
    if (localMusicValues.length > 0) {
        datajson.data.count += localMusicValues.length;
        for (const key in localMusicValues) {
            if (Object.hasOwnProperty.call(localMusicValues, key)) {
                const element = localMusicValues[key];
                let datas = datajson.data.data;
                datas.unshift({ artist: element[0], name: element[1] + "(本地)", local: true, path: element[2] });
            }
        }
    }
}
//遍历读取文件并自动删除大于3天的缓存
function cleanFile(path) {
    fs.readdir(path, (err, files) => {
        if (!err) {
            files.forEach((file) => {
                states = fs.statSync(path + '/' + file);
                let date = new Date();
                let sub = (date - states.birthtime) / 1000 / 60 / 60 / 24
                if (sub && sub > 3) {
                    fs.unlink(path + '/' + file, (err) => {
                        if (err) {
                            console.log(err);
                        }
                    });
                }
            });
        }
    });
}

/**
 * 文件遍历方法
 * @param filePath 当前搜索目录的路径
 * @param libPath 当前媒体库的路径（透传）
 */
function scanMusicFile(filePath, libPath) {
    //根据文件路径读取文件，返回文件列表
    fs.readdir(filePath, function (err, files) {
        if (err) {
            console.warn(err)
        } else {
            if(smfl == 0){
                smfl = files.length; //剩余完成数
            }
            let tsmfl = files.length;
            //遍历读取到的文件列表
            files.forEach(function (filename) {
                //只在首轮遍历中计算“剩余完成数”
                if(files.length == tsmfl){
                    if(smfl > 0){
                        smfl--; //剩余数减1
                        if(smfl == 0){
                            layer.msg("本地音乐库扫描完成！"); 
                        }
                    }
                    
                }
                //获取当前文件的绝对路径
                let filedir = path.join(filePath, filename);
                //根据文件路径获取文件信息，返回一个fs.Stats对象
                fs.stat(filedir, function (eror, stats) {
                    if (eror) {
                        console.warn('获取文件stats失败');
                    } else {
                        let isFile = stats.isFile();//是文件
                        let isDir = stats.isDirectory();//是文件夹
                        if (isFile) {
                            if (filedir.indexOf(".mp3") > 0 || filedir.indexOf(".wma") > 0 || filedir.indexOf(".ape") > 0 || filedir.indexOf(".flac") > 0 || filedir.indexOf(".aac") > 0) {
                                let music = filedir;
                                //读取音乐文件标签信息
                                id3_reader.read(music, function (err, data) {
                                    let yes = false;
                                    if (err == null && data.title != "unknown") {
                                        yes = true;
                                    } else if (data == undefined || data.title == "unknown" && music.indexOf("-") > 0) {
                                        //ID3无法读取的时候，尝试解析文件名
                                        let a = music.substring(music.replace(/\\/g, "/").lastIndexOf("/") + 1, music.indexOf("."));
                                        let b = a.split("-");
                                        if (b.length > 0) {
                                            data = { artist: b[0].trim(), title: b[1].trim() };
                                            yes = true;
                                        }
                                    } else if(music.indexOf("-") < 0 ){
                                        data = { artist: "未知", title: music.trim() };
                                        yes = true;
                                    }
                                    if (yes) {
                                        let a = api.getDB() ? api.getDB().run("delete from local_music_lib where path = ?", [music]) : 0;
                                        //缓存音乐文件位置列表
                                        //添加到“local_music_lib”表
                                        let res = api.getDB() ? api.getDB().run("insert into local_music_lib (artist,title,path, lib_path,can_play ) values (?,?,?,?,?)", [data.artist, data.title, music, libPath, 1]) : 0;
                                        if (res == 0) {
                                            alert(music + "添加失败");
                                        }
                                        api.saveDB();
                                    }
                                });
                            }
                        }
                        if (isDir) {
                            scanMusicFile(filedir, libPath);//递归，如果是文件夹，就继续遍历该文件夹下面的文件
                        }
                    }
                })
            });
        }
    });
}