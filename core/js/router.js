/**
 * 消息路由
 * @author 周毅豪
 */
let events = require("events");

class Router{

    constructor(){
        //创建事件监听的一个对象
        this.emitter = new events.EventEmitter();
    }

    /**
     * 把当前实例放入内存对象
     * @param {string} name 
     */
    putMeToNWStorge(name){
        if(name && name != ''){
            nw.global[name] = this;
        }
    }

    /**
     * 从内存对象取出路由实例
     * @param {string} name 
     */
    getMeFromNWStorge(name){
        if(name && name != ''){
            return nw.global[name];
        }
    }

    regist(name,callbackFunc){
        this.emitter.addListener(name,callbackFunc);
    }

    emit(name,data){
        try {
            this.emitter.emit(name,data)
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = Router;