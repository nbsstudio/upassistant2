/**
 * 内核
 * @author 周毅豪
 * @license GPL v2.0
 */
var fs = require("fs");
let configFileName = 'ccnf.json';
module.exports = {
    state:{
        autoFixdConfigureFile:false //是否已自动修复配置文件，默认不需修复为false值
    },
    alert: function (msg) {
        if(layer != undefined && layer != null){
            layer.alert(msg, {
                skin: 'layui-layer-lan'
                ,closeBtn: 0
                ,anim: 4 //动画类型
            });
        }else{
            alert(msg);
        }
    },
    /**
     * 对比轮询出来的弹幕数据
     * @deprecated
     * @param {*} orgDM 
     * @param {*} newDM 
     */
    compareDanmaku(orgDM, newDM) {
        if (orgDM && newDM) {
            let res = [];
            for (let i in newDM) {
                let ndm = newDM[i];
                let ah = false;
                for (let j in orgDM) {
                    let odm = orgDM[j];
                    if (ndm.sendTimestamp == odm.sendTimestamp) {
                        ah = true;
                        break;
                    }
                }
                if (!ah) {
                    res.push(ndm);
                }
            }
            return res;
        }
    },
    compareWellcomMsg(orgDM, newDM) {
        if (orgDM && newDM) {
            let res = [];
            for (let i in newDM) {
                let ndm = newDM[i];
                let ah = false;
                for (let j in orgDM) {
                    let odm = orgDM[j];
                    if (ndm.userName == odm.userName) {
                        ah = true;
                        break;
                    }
                }
                if (!ah) {
                    res.push(ndm);
                }
            }
            return res;
        }
    },
    copy(obj1) {
        var obj2 = {};
        for (var i in obj1) {
            obj2[i] = obj1[i];
        }
        return obj2;
    },
    readConfig(callback) {
        let ise = fs.existsSync(configFileName);
        if (ise) {
            fs.readFile(configFileName, function (err, data) {
                // console.log("读取文本内容:" + data.toString());
                if (data.length > 0) {
                    let configContent = JSON.parse(data);
                    callback(configContent);
                }
            });
        } else {
            this.initConfig();
            callback({});
        }
    },
    initConfig() {
        let content = '{}';
        fs.writeFile(configFileName, content, function (err) {
        });
    },
    saveAllConfig(data) {
        try {
            let content = JSON.stringify(data);
            if(content != undefined && content != ''){
                fs.writeFile(configFileName, content, function (err) {
                    console.error(err);
                });
            }
        } catch (error) {
            
        }
        
    },
    saveAllConfigBackup(data) {
        let content = JSON.stringify(data);
        fs.writeFile(configFileName+'.bak', content, function (err) {
        });
    },
    checkConfigureFileCopy(data,callback){
        if(this.state.autoFixdConfigureFile){
            return;
        }
        if(data.roomid == undefined || data.roomid == null || data.roomid == '请输入房间号' || data.roomid == '' ){
            if(fs.existsSync(configFileName+'.bak')){
                //可能发生了读取异常，因为主设置里面没有房间号但是却存在配置副本，正常不应发生
                this.alert('配置加载异常，已尝试恢复！');
                //使用副本文件覆盖原配置文件
                fs.copyFileSync(configFileName+'.bak',configFileName);
                this.state.autoFixdConfigureFile = true;
                this.readConfig(callback);
            }
        }
    },
    newSlider(SLIDER_DOM_ID, SLIDER_VALUE_DOM_ID) {
        let slider = document.getElementById(SLIDER_DOM_ID);
        let sliderValue = document.getElementById(SLIDER_VALUE_DOM_ID);

        slider.addEventListener('input', function (e) {
            sliderValue.textContent = e.target.value + '%';
        });

    },
    getOSVersion() {
        var userAgent = navigator.userAgent.toLowerCase();
        var name = 'Unknown';
        var version = 'Unknown';
        if (userAgent.indexOf('win') > -1) {
            name = 'Windows';
            if (userAgent.indexOf('windows nt 5.0') > -1) {
                version = 'Windows 2000';
            } else if (userAgent.indexOf('windows nt 5.1') > -1 || userAgent.indexOf('windows nt 5.2') > -1) {
                version = 'Windows XP';
            } else if (userAgent.indexOf('windows nt 6.0') > -1) {
                version = 'Windows Vista';
            } else if (userAgent.indexOf('windows nt 6.1') > -1 || userAgent.indexOf('windows 7') > -1) {
                version = 'Windows 7';
            } else if (userAgent.indexOf('windows nt 6.2') > -1 || userAgent.indexOf('windows 8') > -1) {
                version = 'Windows 8';
            } else if (userAgent.indexOf('windows nt 6.3') > -1) {
                version = 'Windows 8.1';
            } else if (userAgent.indexOf('windows nt 6.2') > -1 || userAgent.indexOf('windows nt 10.0') > -1) {
                version = 'Windows 10';
            } else {
                version = 'Unknown';
            }
        } else if (userAgent.indexOf('iphone') > -1) {
            name = 'Iphone';
        } else if (userAgent.indexOf('mac') > -1) {
            name = 'Mac';
        } else if (userAgent.indexOf('x11') > -1 || userAgent.indexOf('unix') > -1 || userAgent.indexOf('sunname') > -1 || userAgent.indexOf('bsd') > -1) {
            name = 'Unix';
        } else if (userAgent.indexOf('linux') > -1) {
            if (userAgent.indexOf('android') > -1) {
                name = 'Android';
            } else {
                name = 'Linux';
            }
        } else {
            name = 'Unknown';
        }
        return { name, version };
    }
}

async function blobToArrayBuffer(blob) {
    if ('arrayBuffer' in blob) return await blob.arrayBuffer();
    
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = () => reject;
        reader.readAsArrayBuffer(blob);
    });
}