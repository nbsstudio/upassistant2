/**
 * 内核接口
 * @author 周毅豪
 * @license GPL v2.0
 */
var fs = require("fs");
let db;
// 被创建数据库名称
const dbFileName = "d.sqlite";
const defaultSpeakQty = 500;
let speaking= 'I'; // I为空闲，D为正在朗读，R为准备朗读
let token = null;
let speakQty = defaultSpeakQty; //免费版语音朗读个人配额
let speakQtyBak = defaultSpeakQty;
let prevSpeakTimestamp = 0; //上次朗读时间戳
let dbConfig = {
    // 指定加载sql-wasm.wasm文件的位置
    locateFile: filename => `./${filename}`
};
let speakQueue = [];
module.exports = {
    init(){
        window.API = this; //方便内嵌网页调用
        prevSpeakTimestamp = Date.now();
    },
    createDBInstance(callback) {
        //在html文件中已经导入sql-wasm.js了，这里可以直接初始化数据库实例
        initSqlJs(dbConfig).then(SQL => {
            if (fs.existsSync(dbFileName)) {
                let filebuffer = fs.readFileSync('d.sqlite');
                // 读取数据库
                db = new SQL.Database(filebuffer);
                res = db.exec("SELECT * FROM test");
                if(this.createTableIfNotExists()){
                    fs.writeFileSync(dbFileName, filebuffer);
                }
            } else {
                // 创建数据库
                db = new SQL.Database();
                 // 运行查询而不读取结果
                db.run("CREATE TABLE test (col1, col2);");
                this.createTables(db);
                 // 插入两行：(1,111) and (2,222)
                db.run("INSERT INTO test VALUES (?,?), (?,?)", [1, 111, 2, 222]);
                 // 将数据库导出到包含SQLite数据库文件的Uint8Array
                // export() 返回值: ( Uint8Array ) — SQLite3数据库文件的字节数组
                let data = db.export();
                // 由于安全性和可用性问题，不建议使用Buffer()和new Buffer()构造函数
                // 改用new Buffer.alloc()、Buffer.allocUnsafe()或Buffer.from()构造方法
                // let buffer = new Buffer(data);
                let buff = Buffer.from(data, 'binary');
                fs.writeFileSync(dbFileName, buff);
            }
            callback();
        });
    },
    /**
     * 第一次初始化的时候创建基础表
     * @description 只适合首次初始化
     * @param {*} db 
     */
    createTables(db){
        db.run(`
        create table dmr
        (
            id integer
                constraint dmr_pk
                    primary key autoincrement,
            user_name text not null,
            user_level text,
            user_title text,
            content text not null,
            receive_time datetime not null,
            send_time datetime,
            start_mark smallint default 0
        );`);
        db.run(`
        create table gfr
        (
            id integer
                constraint gfr_pk
                    primary key autoincrement,
            user_name text not null,
            user_id integer,
            gift_name text not null,
            gift_count integer not null,
            gift_action smallint default 0,
            receive_time datetime not null,
            send_time datetime,
            start_mark smallint default 0
        );`);
        db.run(`
        create table wcr
        (
            id integer
                constraint wcr_pk
                    primary key autoincrement,
            user_name text not null,
            user_id integer,
            user_level text,
            user_title text,
            user_action smallint default 0,
            receive_time datetime not null,
            send_time datetime,
            start_mark smallint default 0
        );`);
        this.createTableIfNotExists();
    },
    /**
     * 检查并创建表
     * @description 2020-08-23之后开始的所有表创建都在这个方法内插入
     */
    createTableIfNotExists() {
        const sqlBase = 'SELECT count(*) FROM sqlite_master WHERE tbl_name = ';
        let data = db.exec(sqlBase + `'my_music'`);
        let c = data[0].values[0][0];
        let executed = false;
        if (c == 0) {
            console.log("创建my_music表");
            db.run(`
            create table my_music
            (
                id integer
                    constraint my_music_pk
                        primary key autoincrement,
                artist text not null,
                name text not null,
                range integer,
                music_id integer
            );`);
            executed = true;
        }
        data = db.exec(sqlBase + `'local_music_lib'`);
        c = data[0].values[0][0];
        if (c == 0) {
            console.log("创建local_music_lib表");
            db.run(`
            create table local_music_lib (
                id integer
                    constraint local_music_lib_pk
                        primary key autoincrement,
                artist text not null,
                title text not null,
                path text not null,
                lib_path text not null,
                lyrics_path text,
                can_play integer not null
            );`);
            executed = true;
        }
        return executed;
    },
    dispatch(danmaku, type,config) {
        if(config.enableBaiduSpeak){
            this.pushToQueue(danmaku,type,config);
        }
        if(config.enableSTS){
            this.insertRec(danmaku,type);
        }
        if(type == 'dm'){
            if(danmaku.content.indexOf('点歌 ')==0){
                window.vfpsAdd(danmaku.content.replace('点歌 ',''));
            }
        }
        
    },
    pushToQueue(danmaku,type,config){
        //有限制地插入消息至队列
        if(speakQueue.length <= 10){
            speakQueue.push({dk:danmaku,type:type});
        }
        //空闲状态下触发朗读
        if('I' === speaking && speakQueue.length > 0){
            let speakObj = speakQueue.shift(); //取走队列消息
            this.baiduSpeak(speakObj.dk,speakObj.type,config,true);
        }
    },
    baiduSpeak(danmaku,type,config){
        this.baiduSpeak(danmaku,type,config,false);
    },
    baiduSpeak(danmaku,type,config,playQueueList){
        if ('D' === speaking) {
            return;
        }
        let isCus = false; //是否再用自定义KEY
        //检查是否使用免费额度还是自定义API_KEY
        if (config.hasOwnProperty('upgradePlanSet') && config.upgradePlanSet.hasOwnProperty('enableCustomBaiduApi') && config.upgradePlanSet.enableCustomBaiduApi) {
            speakQty = 9999; //额度永不消耗
            isCus = true;
        } else {
            isCus = false;
            if (speakQty <= 0) {
                return;
            }
        }
        speaking = 'D';
        let voiceVol = 3;
        let speed = 4;
        let person = 4;
        let url = "";
        let includeSender = true;
        let apiKey= "EPfGtLrj3Ho73OAeR2a5C56F";
        let apiSecert = "974159fece8d48f607a376115387baa4";
        let _this = this;
        try {
            if(config.hasOwnProperty('voiceVol') && config.voiceVol){
                voiceVol = parseInt(config.voiceVol);
            }
            if(config.hasOwnProperty('speakPerson') && config.speakPerson){
                person = parseInt(config.speakPerson);
            }
            if(config.hasOwnProperty('voiceSpeed') && config.voiceSpeed){
                speed = parseInt(config.voiceSpeed);
            }
            if(config.hasOwnProperty('includeSender')){
                includeSender = config.includeSender;
            }
            if(isCus){
                apiKey = config.upgradePlanSet.baiduApiKey;
                apiSecert = config.upgradePlanSet.baiduApiSecert;
            }
        } catch (error) {
            
        }
        if (token) {
            let speech = danmaku.content;
            if(includeSender && type !== 'wellcome'){
                speech = danmaku.userName+'吼，' + speech;
            }
            url = "http://tsn.baidu.com/text2audio?lan=zh&ctp=1&cuid=abcdxxx&tok=" + token + "&tex=" + encodeURI(speech) + "&vol=" + voiceVol + "&per=" + person + "&spd=" + speed + "&pit=5&aue=3";
            try {
                let audio = new Audio(url);
                audio.src = url;
                audio.volume = voiceVol / 10;
                audio.play();
                audio.onplaying = function () {
                    prevSpeakTimestamp = Date.now();
                    speaking = 'D';
                };
                audio.onended = function () {
                    speaking = 'I';
                    //每说一句话扣一次额度
                    speakQty--;
                    if(!isCus){
                        speakQtyBak = speakQty;
                    }
                    if(playQueueList && speakQueue.length > 0){
                        //朗读完之后取出下一条消息继续朗读
                        let speakObj = speakQueue.shift();
                        _this.baiduSpeak(speakObj.dk,speakObj.type,config,true);
                    }
                }
            } catch (error) {
                speaking = false;
            }
        } else {
            speaking = 'R'; //准备朗读状态
            $.post("https://openapi.baidu.com/oauth/2.0/token", "grant_type=client_credentials&client_id=" + apiKey + "&client_secret=" + apiSecert, function (data) {
                token = data.access_token;
                _this.baiduSpeak(danmaku, type,config,playQueueList);
            });
        }
    },
    getSpeakQty(){
        return speakQty;
    },
    resetSpeakQty(){
        speakQty = speakQtyBak; //重置为修改前的额度（防止用户利用修改增值页面的KEY得到无限额度之后再去掉自定KEY来骗取额度）
    },
    /**
     * 插入统计纪录
     * @param {*} danmaku 
     * @param {*} type 
     */
    insertRec(danmaku,type){
        switch (type) {
            case 'dm':
                db.run("insert into dmr (user_name, user_level, user_title, content, receive_time, send_time) values (?,?,?,?,datetime('now','localtime'),datetime(?))", [danmaku.userName, danmaku.userLevel, '', danmaku.content, danmaku.sendTimestamp]);
                break;
            case 'wc':
                db.run("insert into wcr (user_name, user_id, user_level, user_title, receive_time) values (?,?,?,?,datetime('now','localtime'))", [danmaku.userName, '', '','']);
                break;
            case 'gf':
                db.run("insert into gfr (user_name, user_id, gift_name, gift_count, receive_time, send_time) values (?,?,?,?,datetime('now','localtime'),datetime(?))", [danmaku.userName, '', danmaku.giftName, danmaku.count, danmaku.sendTimestamp]);
                break;
            case 'tgf':
                db.run("insert into gfr (user_name, user_id, gift_name, gift_count, receive_time, send_time) values (?,?,?,?,datetime('now','localtime'),datetime(?))", [danmaku.userName, '', danmaku.giftName, danmaku.count, danmaku.sendTimestamp]);
                break;
            default:
                break;
        }
    },
    flushRec(useAsync){
        let data = db.export();
        // let buffer = new Buffer(data);
        let buff = Buffer.from(data, 'binary');
        if(useAsync){
            fs.writeFile(dbFileName, buff,(err) => {

            });
        }else{
            fs.writeFileSync(dbFileName, buff);
        }
    },
    getDB(){
        if(db){
            return db;
        }else{
            return false;
        }
    },
    saveDB(){
        if(db){
            this.flushRec(true);
        }
    }
}