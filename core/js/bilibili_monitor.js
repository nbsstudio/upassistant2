/**
 * 直播间监视窗口
 * @author 周毅豪
 * @license GPL v2.0
 */

/**
 * 对于弹幕爬虫的监视窗口进行操作
 */
let document = null;
module.exports = {
    init(doc,canClose){
        if(canClose == undefined){
            canClose = false;
        }
        document = doc;
        if(document){
            let _this = this;
            $(document).ready(function(){
                _this.addTitle()
                if(canClose){
                    _this.addCloseButton()
                }
                _this.removeSomething();
                _this.styleSomething();
            });
        }
    },
    addTitle(){
        $(document).append('<div id="nbs" style="position: fixed;width:100%;height: 30px;background-color:rgb(248,59,108);z-index: 9999;top: 0px;">'+
        '<div style="position: absolute;height: 100%;width: 90%;color: white;">'+
        '<span style="font-size: 23px">直播间监视窗口</span>'+
        '</div>'+
        '<div style=""></div>'+
        '</div>');
        $(document).append('<div id="tips1" style="position: fixed;width:100%;height: 30px;background-color:white;opacity:80%;z-index: 9999;top: 30px;">'+
        '<div id="tips1_body" style="position: absolute;height: 100%;width: 100%;color: black;">'+
        '<marquee><span style="font-size: 23px">如果你不想听到自己直播间的声音，可以在播放器中点击<b>小喇叭</b>静音！感谢支持！</span></marquee></div>'+
        '</div>');
    },
    addCloseButton(){
        $(document).find("#nbs").append('<div id="nbs" style="position: fixed;width:90px;height: 30px;top: 0px;right:0px;">'+
        '<button style="color:white;width: 100%;height: 100%;vertical-align: middle;align-content: center;line-height: 100%;background-color: rgb(0, 200, 255);border: none;cursor: pointer;z-index: 9999;" href="#">alt+F4可关闭</button>'+
        '</div>');
    },
    removeSomething(){
        $(document).find('#link-navbar-vm').remove();
        $(document).find('#sidebar-vm').remove();
        $(document).find('#my-dear-haruna-vm').remove();
        $(document).find('#gift-control-vm').remove();
        $(document).find('#sections-vm').remove();
        $(document).find('.footer-right').remove();
        $(document).find('.partner-banner').remove();
    },
    styleSomething(){
        $(document).find('.player-and-aside-area').css('width','1000px');
        $(document).css('overflow','hidden');
        $(document).find('#nbs').css('-webkit-app-region','drag');//使窗口可以在无边框的状态下任意移动
        $(document).find('#tips1').css('-webkit-app-region','drag');//使窗口可以在无边框的状态下任意移动
        $(document).find('#aside-area-vm').css('display','none');
    }
}