const LiveFlow = require("../lib/shanmite/live_flow");
const Router = require("./router");
const { messageHandle } = require("../lib/shanmite/message_handle");
//内部属性
let bw = null;
// let ordkEvent = function (dmk) { };//弹幕回调；20210613 removed，用更为规范化的路由写法来传递消息，这个对象相应废止
// let orgEvent = function (dmk) { };//小礼物回调
// let ortgEvent = function (dmk) { };//大礼物回调
let router = new Router();
let prevGift = { text: "", time: 0 };
let Monitor = null;
let cacheT = [];

module.exports = {
    running: false,
    tab: {},
    bilibili: {},
    document: {},
    //核心属性
    timeOut: false,//是否超时
    waitGift: false,
    hideWindow: false,
    dkOsv: {},
    gfOsv: {},
    instant: null,
    roomid:"",
    init(monitorEntity){
        Monitor = monitorEntity;
    },
    openOne(roomid, hide, recivingGiftEvent, recivingTitanGiftEvent, recivingDkEvent) {
        // ordkEvent = recivingDkEvent;
        if(recivingDkEvent){
            router.regist('ordkEvent',recivingDkEvent); //20210613 added,用更为规范化的路由写法来传递消息
        }
        // orgEvent = recivingGiftEvent;
        if(recivingGiftEvent){
            router.regist('orgEvent',recivingGiftEvent); //20210613 added,用更为规范化的路由写法来传递消息
        }
        // ortgEvent = recivingTitanGiftEvent;
        if(recivingTitanGiftEvent){
            router.regist('ortgEvent',recivingTitanGiftEvent); //20210613 added,用更为规范化的路由写法来传递消息
        }

        if (!this.instant) {
            this.instant = new LiveFlow();
        }
        this.running = true;
        this.roomid = roomid;
        let ft = this;
        this.instant
            .setRoomId(Number(roomid))
            .setMessageHandle((msg) => {
                //定义消息接收事件
                if (msg.type === 'MESSAGE') {
                    msg.inner.forEach((t) => {
                        //传递回调函数到消息句柄
                        t.callbackFunc = ft.onMessage;
                        //传递消息
                        messageHandle(t);
                    });
                }
            })
            .setPipemsgHandle(this.onPipemsgCom)
            .setErrorHandle(this.onCoreError)
            .run();
        //对监视窗口的操作
        this.timeOut = false;//重置是否超时的值
        let _this = this;
        //先打开核心部件，再跳转至B站可以解决窗口加载因网络慢导致迟迟未能触发‘loaded’事件的问题。
        nw.Window.open('bw/rdbw.html', { frame: false, show_in_taskbar: false }, function (win) {
            win.on('loaded', function () {
                win.resizeTo(720, 591);
                win.setResizable(false);
                bw = win;
                //命令跳转至B站
                nw.global.redirect(roomid);
                _this.tab = win.cWindow.tabs[0];
                _this.document = win.window.document;
                _this.bilibili = win.window.document.body;
                Monitor.init(win.window.document.body,true);//打开一个可关闭的监控窗口
            });
            // win.on('close', function () {
            //     return true;
            // })
        });
    },
    getMyInfo() {
        let res = {};
        if ($(this.bilibili).find('.header-info-ctnr').length > 0) {
            let avatar = $(this.bilibili).find('.header-info-ctnr .anchor-avatar');
            let cover = $(this.bilibili).find('.header-info-ctnr .room-cover');
            res.url = cover.attr('href');
            res.avatarUrl = avatar.attr('style').replace('background-image: url("', '').replace('");', '');
        }else if($(this.bilibili).find('#head-info-vm').length > 0){
            let avatar = $(this.bilibili).find('.header-info-ctnr .anchor-avatar');
            let cover = $(this.bilibili).find('.header-info-ctnr .room-cover');
            res.url = cover.attr('href');
            res.avatarUrl = avatar.attr('style').replace('background-image: url("', '').replace('");', '');
        }
        return res;
    },
    getWellcomeMsg() {

    },
    cacheTGift() {

    },
    onMessage(shanmiteObject) {
        //转换为UpassistantStandard2.x的对象
        if(shanmiteObject.isGift){
            if(shanmiteObject.titan){
                // ortgEvent(shanmiteObject);
                router.emit("ortgEvent",shanmiteObject);
            }else{
                // orgEvent(shanmiteObject);
                router.emit("orgEvent",shanmiteObject);
            }
        }else{
            // ordkEvent(shanmiteObject);
            router.emit("ordkEvent",shanmiteObject);
        }
    },
    getIsConnTimeOut() {
        return this.timeOut;
    },
    shutdown() {
        this.instant.close();
        if(bw){
            bw.close(true);
        }
    },
    showMonitor() { },
    reconn() {
        this.shutdown();
        this.openOne(this.roomid,false,null,null,null) //20210613 modifly,有了路由对象，就不需要再
    },
    onCoreError(codeStr){
        //引擎核心报错，转译报错信息
        let msg = "";
        switch (codeStr) {
            case "live_flow.initialize.failed":
                msg = "shanmite弹幕引擎启动失败！";
                break;
            case "live_flow.run.failed":
                msg = "shanmite弹幕引擎启动失败,无法连接！";
                break;
            case "live_flow.web_socket.error":
                msg = "意外中断了直播间的连接！";
                break;
        
            default:
                break;
        }
        let dmk = {
            userName: "系统",
            sendTimestamp: Date.parse(new Date()),
            userLevel: "9999",
            userRank: "无法获取",
            content: msg,
            isGift: false,
        };
        // ordkEvent(dmk);
        router.emit("ordkEvent",dmk);
    },
    onPipemsgCom(msg){
        switch (msg) {
            case "live_flow.close":
                console.log('关闭连接');
                break;
            case "live_flow.connected":
                console.log('成功连接');
                let dmk = {
                    userName: "系统",
                    sendTimestamp: Date.parse(new Date()),
                    userLevel: "9999",
                    userRank: "无法获取",
                    content: "成功连接，房间ID："+this.roomid,
                    isGift: false,
                };
                // ordkEvent(dmk);
                router.emit("ordkEvent",dmk);
                break;
            default:
                break;
        }
    }
}