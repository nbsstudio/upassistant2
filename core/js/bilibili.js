const Router = require("./router");
/**
 * 弹幕接收系统1.0
 * @deprecated
 * @author 周毅豪
 * @license GPL v2.0
 */
//内部属性
let bw = null;
// let ordkEvent = function(dmk){};//弹幕回调
// let orgEvent = function(dmk){};//小礼物回调
// let ortgEvent = function(dmk){};//大礼物回调
let router = new Router();
let prevGift = {text:"",time:0};
let Monitor = null;
let cacheT = [];

module.exports = {
    running:false,
    tab:{},
    bilibili: {},
    document:{},
    //核心属性
    timeOut:false,//是否超时
    waitGift:false,
    hideWindow:false,
    readyState:true,
    dkOsv:{},
    gfOsv:{},
    wmsgOsv:{},
    temp:{preWmsg:''},
    init(monitorEntity){
        Monitor = monitorEntity;
        router.putMeToNWStorge("bilibili");
    },
    openOne(roomid,hide,recivingGiftEvent,recivingTitanGiftEvent,recivingDkEvent,recivingWellcomeEvent) {
        this.timeOut = false;//重置是否超时的值
        let _this = this;
        this.readyState = true;//设置准备状态
        //先打开核心部件，再跳转至B站可以解决窗口加载因网络慢导致迟迟未能触发‘loaded’事件的问题。
        nw.Window.open('bw/rdbw.html', {frame: false, show_in_taskbar: false,icon:'logo2.png',title:'live-room-viewer'}, function (win) {
            win.on('loaded', function () {
                win.resizeTo(720,591);
                win.setResizable(false);
                bw = win;
                //命令跳转至B站
                nw.global.redirect(roomid);
                if(recivingDkEvent){
                    router.regist('ordkEvent',recivingDkEvent); //20210718 added,用更为规范化的路由写法来传递消息
                }
                // orgEvent = recivingGiftEvent;
                if(recivingGiftEvent){
                    router.regist('orgEvent',recivingGiftEvent); //20210718 added,用更为规范化的路由写法来传递消息
                }
                // ortgEvent = recivingTitanGiftEvent;
                if(recivingTitanGiftEvent){
                    router.regist('ortgEvent',recivingTitanGiftEvent); //20210718 added,用更为规范化的路由写法来传递消息
                }
                if(recivingWellcomeEvent){
                    router.regist('orwEvent',recivingWellcomeEvent); 
                }
                _this.running = true;
                _this.tab = win.cWindow.tabs[0];
                _this.document = win.window.document;
                _this.bilibili = win.window.document.body;
                Monitor.init(win.window.document.body);
                win.minimize();
                _this.bindWellcomMsgReader();
                _this.bindDKReader();
                _this.bindGiftReader();
                setTimeout(() => {
                    //成功连接房间后依然延迟0.5秒再设置为非准备状态
                    _this.readyState = false; 
                }, 3000);
            });
            win.on('close',function(){
                win.minimize();
                _this.hideWindow = true;
            })
        });
        //15秒后检查是否超时无响应了
        setTimeout(() => {
            if(_this.getMyInfo()){
                _this.timeOut = false;
            }else{
                //连接超时
                _this.readyState = true; 
                _this.timeOut = true;
                _this.waitGift = false;
                //关闭窗口并复位
                _this.shutdown();
                // bw.close(true);
            }
        }, 15000);
    },
    getMyInfo(){
        if($(this.bilibili).find('.header-info-ctnr').length > 0){
            let res = {};
            let avatar = $(this.bilibili).find('.header-info-ctnr .anchor-avatar');
            let avatarNew = $(this.bilibili).find('.header-info-ctnr .avatar');
            if(avatarNew && avatarNew.length > 0){
                avatar = avatarNew;
            }
            let cover = $(this.bilibili).find('.header-info-ctnr .room-cover');
            res.url = cover.attr('href');
            if(avatar && avatar.length > 0){
                if(avatar.attr('style') != undefined){
                    res.avatarUrl = avatar.attr('style').replace('background-image: url("','').replace('");','');
                }else{
                    res.avatarUrl = avatar.find('.blive-avatar-face').css('background-image').replace('url("','').replace('")','');
                }
                
            }
            return res;
        }
    },
    // cacheTGift(giftArray){
    //     //如果跟缓存比较，礼物数量多了，取多出来的部分通知前端
    //     let isBigger = giftArray.length > cacheT.length; 
    //     if(isBigger){
    //         let x = giftArray.length - cacheT.length;
    //         let newGiftArray = giftArray.slice(0-x)[0];
    //         newGiftArray.titan = true;
    //         // ortgEvent(newGiftArray);
    //         router.emit("ortgEvent",newGiftArray);
    //     }else{
    //         //如果数量相同，则比较最后一个元素，不相同的话就证明是新的礼物
    //         let a = giftArray.slice(-1)[0];
    //         let b = cacheT.slice(-1)[0];
    //         if(a.userName != b.userName || a.giftName != b.giftName || a.count != b.count){
    //             let newGiftArray = a;
    //             newGiftArray.titan = true;
    //             // ortgEvent(newGiftArray);
    //             router.emit("ortgEvent",newGiftArray);
    //         }
    //     }
    //     cacheT = giftArray;

    // },
    bindWellcomMsgReader(){
        if(this.bilibili){
            let promptFrame = $(this.bilibili).find(".chat-history-panel .brush-prompt");
            let _this = this;
            if(promptFrame.length > 0){
                let callback = function(mutationsList) {
                    let wmsgc = mutationsList[0];
                    if(wmsgc.type == "childList" && wmsgc.target.outerText != ""){
                        if(wmsgc.target.outerText !== _this.temp.preWmsg){ //防止重复事件
                            let dkObj = {
                                // debug:$(this),
                                userName:'助手',
                                sendTimestamp: 0,
                                userLevel: "N/A",
                                userRank: "N/A",
                                content: '欢迎'+wmsgc.target.outerText,
                                isGift: false,
                                supper:false
                            };
                            if(_this.readyState == false){
                                router.emit("orwEvent",dkObj);
                            }
                            _this.temp.preWmsg = wmsgc.target.outerText;
                        }
                    }
                };
                this.wmsgOsv = new MutationObserver(callback);
                // 开始观察已配置突变的目标节点
                this.wmsgOsv.observe(promptFrame[0], { attributes: false, childList: true, subtree: false });
            }
        }
    },
    bindDKReader(){
        if(this.bilibili){
            let chatItemsFrame = $(this.bilibili).find(".chat-items");
            let _this = this;
            if(chatItemsFrame.length > 0){
                let callback = function(mutationsList) {
                    let reciveDate = new Date();
                    //只接收弹出的部分，忽略元素消失时的事件
                    if(mutationsList[0].removedNodes.length == 0){
                        let danmakuNodes = mutationsList[0].addedNodes;
                        let tempGiftArray = [];
                        for (const i in danmakuNodes) {
                            if (danmakuNodes.hasOwnProperty(i)) {
                                let danmaku = danmakuNodes[i];
                                if (danmaku.getAttribute("class").indexOf("danmaku-item") >= 0) {
                                    let supper =  (danmaku.getAttribute("class").indexOf("superChat-card-detail") >= 0);
                                    danmaku = $(danmaku);
                                    let ulvt = danmaku.find('.user-level-icon').text();
                                    let ulv = new RegExp(`UL \\d+`).exec(ulvt);
                                    let ur = new RegExp(`用户排名：>\\d+`).exec(ulvt);
                                    let dmkContent = '';
                                    if(danmaku.find('.danmaku-item-right').length > 0 && danmaku.find('.danmaku-item-left').length >0 ){
                                        dmkContent = danmaku.find('.danmaku-item-right').html();
                                    }else{
                                        dmkContent = danmaku[0].dataset.data-danmaku.text();
                                    }
                                    let dkObj = {
                                        // debug:$(this),
                                        userName: danmaku.attr('data-uname'),
                                        sendTimestamp: danmaku.attr('data-ts'),
                                        userLevel: ulv != null ? ulv[0] : "N/A",
                                        userRank: ur != null ? ur[0] : "N/A",
                                        content: dmkContent,
                                        isGift: false,
                                        supper:false,
                                        reciveDate:reciveDate
                                    };
                                    if(supper){
                                        //超级弹幕（氪金那种）
                                        let cimb = danmaku.find(".card-item-middle-bottom");
                                        dkObj.content = cimb.find("span.text").text();
                                        dkObj.color = cimb.css("background-color");
                                        dkObj.supper = true;
                                    }
                                    if(_this.readyState == false){
                                        router.emit("ordkEvent",dkObj);
                                    }
                                } else if (danmaku.getAttribute("class").indexOf("gift-item") >= 0) {
                                    let gift = $(danmaku);
                                    //在普通弹幕中提取出大礼物的记录
                                    //解析
                                    let giftInfo = {
                                        action: gift.find('.action').text(),
                                        giftName: gift.find('.gift-name').text(),
                                        count: gift.find('.gift-count').text(),
                                    }
                                    let giftObj = {
                                        userName: gift.find('.username').text(),
                                        sendTimestamp: Date.parse(new Date()),
                                        userLevel: '',
                                        content: giftInfo.action + giftInfo.giftName + giftInfo.count,
                                        action: giftInfo.action,
                                        giftName: giftInfo.giftName,
                                        count: giftInfo.count,
                                        isGift: true,
                                        titan:true,
                                        reciveDate:reciveDate
                                    };
                                    tempGiftArray.push(giftObj);
                                    if(_this.readyState == false){
                                        router.emit("ortgEvent",giftObj);
                                    }
                                }
                            }
                        }
                        //缓存大礼物并分析新旧
                        // _this.cacheTGift(tempGiftArray);
                    }
                }
                this.dkOsv = new MutationObserver(callback);
                // 开始观察已配置突变的目标节点
                this.dkOsv.observe(chatItemsFrame[0], { attributes: false, childList: true, subtree: false });
            }
        }
    },
    bindGiftReader(){
        if(this.bilibili){
            let pgm = $(this.bilibili).find('#penury-gift-msg');
            let _this = this;
            if(pgm.length > 0){
                let callback = function(mutationsList) {
                    //只接收弹出的部分，忽略元素消失时的事件
                    if(mutationsList[0].removedNodes.length == 0){
                        let danmaku = mutationsList[0].target.children[0];
                        if(danmaku.getAttribute("class").indexOf("a-move-in-top")>0){
                            //考虑到观察员模式对变动过于敏感，每次进来这个方法都要重复检查
                            if(prevGift.text != danmaku.outerText){
                                let userName = pgm.find("span[class^='username']").text();
                                let action = pgm.find("span[class^='action']").text();
                                let giftName = pgm.find("span[class^='gift-name']").text();
                                let count = pgm.find("span[class^='count']").text();
                                _this.onGiftReciving(userName,action,giftName,count);
                            }
                            prevGift = {
                                text:danmaku.outerText,
                                time:Date.parse(new Date())
                            };
                        }else if(danmaku.getAttribute("class").indexOf("a-move-in-bottom")>0){
                            //当出现"a-move-in-bottom"指令时，让下一条礼物不需查重、因为有可能是观众老爷在疯狂刷礼物
                            prevGift = {
                                text:"",
                                time:0
                            };
                        }
                    }
                }
                this.gfOsv = new MutationObserver(callback);
                // 开始观察已配置突变的目标节点
                this.gfOsv.observe(pgm[0], { attributes: false, childList: true, subtree: false });
            }
        }
    },
    onGiftReciving(userName,action,giftName,count){
        let dmk = {
            userName:userName,
            sendTimestamp:Date.parse(new Date()),
            userLevel:"",
            content:userName+action+count+giftName,
            isGift:true,
            action:action,
            giftName:giftName,
            count:count,
            titan:false
        };
        if(this.readyState == false){
            router.emit("orgEvent",dmk);
        }
    },
    getIsConnTimeOut(){
        return this.timeOut;
    },
    shutdown(){
        if(bw){
            if(this.dkOsv && this.gfOsv && this.dkOsv.hasOwnProperty('disconnect') && this.gfOsv.hasOwnProperty('disconnect')){
                this.dkOsv.disconnect();
                this.gfOsv.disconnect();
            }
            bw.close(true);
        }
    },
    showMonitor(){
        if(this.hideWindow){
            bw.restore();
        }
    },
    reconn(){
        if(bw){
            bw.reload();
        }
    }

}

