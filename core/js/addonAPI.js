/**
 * 插件内部API
 * @author 周毅豪
 * @license GPL v2.0
 */
var fs = require("fs");
const Router = require("./router");
const publicAPI = require("./addonPublicAPI");
const PLUGIN_DIR = 'up2_plugins';
let instances = [];

module.exports = {
    h: 0,
    routerObject: null,
    api:null,
    checkPlugin(checkedOk){
        try {
            if(!this.api.getDB()){
                let _this = this;
                setTimeout(() => {
                    _this.checkPlugin(checkedOk);
                }, 500);
            }
            let data = this.api.getDB() ? this.api.getDB().exec("select tbl_name from sqlite_master where tbl_name = 'my_plugin'") : [];
            console.log("插件库自检...")
            if (data.length > 0) {
                checkedOk();
            }else{
                this.createPluginTable();
            }
        } catch (error) {
            console.error(error);
        }
    },
    //用户首次使用时，创建对应的表
    createPluginTable(){
        let ddl = `create table my_plugin
        (
            title text not null
                constraint my_plugin_pk
                    primary key,
            author text,
            icon_path text,
            folder_path text,
            version text not null,
            ua_version text,
            platform text,
            from_url text,
            type int,
            remark text,
            script text,
            sort int
        );`;
        try {
            let res = this.api.getDB() ? this.api.getDB().run(ddl) : 0;
            if(res !== 0){
                this.api.saveDB();
            }else{
                this.createPluginTable(); //重试
            }
        } catch (error) {
            console.error("无法创建插件表！"+error);
        }
    },
    loadList(){
        let data = this.api.getDB() ? this.api.getDB().exec("select title, author, icon_path, folder_path, version, ua_version, platform, from_url, type, remark, script, sort from my_plugin") : [];
        return data;
    },
    init(coreApiObj) {
        let bilibiliCore = new Router().getMeFromNWStorge("bilibili");
        if (this.routerObject == null && bilibiliCore) {
            this.routerObject = bilibiliCore;
            this.routerObject.regist('ordkEvent', this.dispatch);
            this.routerObject.regist('orgEvent', this.dispatch);
            this.routerObject.regist('ortgEvent', this.dispatch);
            this.routerObject.regist('orwEvent', this.dispatch);
        }
        this.api = coreApiObj;
    },
    dispatch(danmaku, type) {
        instances.forEach(element => {
            try {
                element.instance.msg(danmaku,type);
            } catch (error) {
                console.error(error.message);
            }
        });
    },
    loadPlugin(title,version){
        try {
            //动态加载js模块并返回模块对象
            let poj = eval("require('../'+PLUGIN_DIR+'/'+title+'/main.js');");
            if(poj.init != undefined && poj.init != null){
                poj.init(publicAPI);
            }
            instances.push({title:title,version:version,instance:poj});
            return poj;
        } catch (error) {
            console.error(error.message);
            return null;
        }
    }
}