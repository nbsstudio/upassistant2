/**
 * 弹幕接收内核，感谢shanmite大神的开源
 * @author shanmite
 */
// const WebSocket = require("ws");
const { log } = require("./base");
const Convert = require("./convert");
const LiveAPI = require("./live_api");

class ServerInfo {
    constructor() {
        this.roomid = 0;
        this.uid = 1;
        this.wss_url = '';
        this._hostList = [];
        this.userAuthentication = {
            uid: 1,
            roomid: 0,
            protover: Convert.WS.BODY_PROTOCOL_VERSION_DEFLATE,
            platform: "web",
            clientver: "2.6.42",
            type: 2,
            key: ''
        };
    }

    /**
     * 设置直播房间号
     * @param {number} rid
     * @returns 
     */
    setRoomId(rid) {
        this.roomid = rid;
        return this
    }

    /**
     * 设置自己的uid(可随意设置)
     * @param {number} uid
     * @returns 
     */
    setUid(uid) {
        this.uid = uid;
        return this
    }

    _setWssURL(wss_url) {
        this.wss_url = wss_url;
    }

    *hostListIter() {
        for (const host of this._hostList) {
            yield host
        }
    }

    async initInfo() {
        let result = true;
        try {
            const DanmuInfo = await LiveAPI.DanmuServer(this.roomid);
            this.userAuthentication.key = DanmuInfo.token;
            this.userAuthentication.roomid = this.roomid;
            this.userAuthentication.uid = this.uid;
            this._hostList = DanmuInfo.host_list.map(it => {
                return 'wss://' + it.host + ':' + it.wss_port + '/sub'
            })
        } catch (error) {
            log.err(error);
            result = false;
        }
        return result
    }
}

class LiveFlow extends ServerInfo {
    constructor() {
        super();
        this.heartBeat = 0;
        this.HostList = null;
        this.option = {
            retryTimeOut: 1 * 1000,
            heartBeatInterval: 30 * 1000,
        }
        this.connectState = false;
        this._messageHandle = () => {}; //消息处理句柄
        this._errorHandle = () => {}; //错误传递句柄
        this._pipemsgHandle = () => {}; //内部消息传递句柄
    }

    /**
     * @typedef MSG
     * @property {"MESSAGE"|"POPULAR_COUNT"|"CONNECT_SUCCESS"} type
     * @property {Array|number} inner
     * @param {MessageHandle} cb
     * @returns {this}
     * @callback MessageHandle
     * @param {MSG} msg
     */
    setMessageHandle(cb) {
        this._messageHandle = cb;
        return this;
    }

    /**
     * 出错处理
     * @param {*} cb 
     */
    setErrorHandle(cb){
        this._errorHandle = cb;
        return this;
    }
    
    /**
     * 内部消息处理
     * @param {*} cb 
     */
    setPipemsgHandle(cb){
        this._pipemsgHandle = cb;
        return this;
    }

    async run() {
        if (this.connectState) {
            this._wsConnect()
        } else {
            try {
                if (this.roomid && await this.initInfo()) {
                    log.ok('live_flow -> initialize success');
                    this.HostList = this.hostListIter();
                    this._wsConnect()
                } else {
                    this._errorHandle('live_flow.initialize.failed');
                }
            } catch (error) {
                log.err(error);
                this._errorHandle('live_flow.run.failed');
            }
        }
    }

    close() {
        if(this.ws){
            this.ws.close();
        }
    }

    _wsConnect() {
        if (!this.connectState) {
            const { value, done } = this.HostList.next();
            if (!done) this._setWssURL(value);
        }
        this.ws = new WebSocket(this.wss_url);
        this.ws.onopen = this.onOpen.bind(this);
        this.ws.onmessage = this.onMessage.bind(this);
        this.ws.onerror = this.onError.bind(this);
        this.ws.onclose = this.onClose.bind(this);
    }

    _heartBeat() {
        return setInterval(() => {
            if(this.connectState){
                log.ok('live_flow -> heartBeat');
                this.ws.send(
                    Convert.toArrayBuffer(
                        {}.toString(),
                        Convert.WS.OP_HEARTBEAT
                    )
                )
            }
        }, this.option.heartBeatInterval);
    }

    _parse(raw_data_toArrayBuffer,msg) {
        const { totalLen, body, op, popular_count } = Convert.toObject(raw_data_toArrayBuffer);
        if (totalLen > 0) {
            msg.callbackFunc = this._messageHandleCallback;
            const { WS } = Convert;
            switch (op) {
                case WS.OP_MESSAGE:
                    msg.type = 'MESSAGE';
                    msg.inner = body;
                    break;
                case WS.OP_HEARTBEAT_REPLY:
                    msg.type = 'POPULAR_COUNT';
                    msg.inner = popular_count;
                    log.ok('当前人气值: ' + popular_count);
                    break;
                case WS.OP_CONNECT_SUCCESS:
                    if (body[0].code === WS.AUTH_OK) {
                        log.ok('live_flow -> connect success ' + `(roomid: ${this.roomid})`);
                        msg.type = 'CONNECT_SUCCESS';
                        this.connectState = true;
                        this.heartBeat = this._heartBeat();
                        //通知主界面，连接成功了
                        if(this._pipemsgHandle){
                            this._pipemsgHandle('live_flow.connected');
                        }
                    }
                    else if (body[0].code === WS.AUTH_TOKEN_ERROR) {
                        this.close();
                    }
                    break;
                default:
                    this.close();
                    return;
            }
            this._messageHandle(msg);
        }
    }

    onOpen() {
        log.ok('live_flow -> connent to ' + this.wss_url);
        this.ws.send(
            Convert.toArrayBuffer(
                JSON.stringify(this.userAuthentication),
                Convert.WS.OP_USER_AUTHENTICATION
            )
        );
    }

    onMessage(raw_msg) {
        // this.connectState = true;
        let raw_data_toArrayBuffer = null;
        let msg = { type: '' };
        const raw_data = raw_msg.data;
        if (raw_data instanceof Buffer) {
            raw_data_toArrayBuffer = raw_data.buffer.slice(raw_data.byteOffset, raw_data.byteOffset + raw_data.byteLength)
        } else if (raw_data instanceof ArrayBuffer) {
            raw_data_toArrayBuffer = raw_data
        } else if (raw_data instanceof Blob){
            const reader = new FileReader();
            let parser = this;
            reader.onload = function(){
                parser._parse(reader.result,msg);
            };
            reader.onerror = function(e){
                log.err(e)
            };
            reader.readAsArrayBuffer(raw_data);
            return;
        }
        this._parse(raw_data_toArrayBuffer,msg);
    }

    onError(err) {
        log.err(err.message);
        this._errorHandle('live_flow.web_socket.error');
    }

    onClose() {
        if (this.connectState) {
            clearInterval(this.heartBeat);
            this.connectState = false;
            if(this._pipemsgHandle){
                this._pipemsgHandle('live_flow.close');
            }
        } else {
            log.ok('live_flow -> change host');
            setTimeout(this._wsConnect.bind(this), this.option.retryTimeOut)
        }
    }
}


module.exports = LiveFlow;