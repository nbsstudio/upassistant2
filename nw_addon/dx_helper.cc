#include <node.h>
#include <iostream>
#include <tchar.h>
#include <Windows.h>
#include <WinUser.h>
#include "utils.hpp"

#define UNUSED_PARAMETER(param) (void)param

namespace main
{

    using v8::Exception;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Number;
    using v8::Object;
    using v8::String;
    using v8::Value;

    struct Injetor
    {
        HANDLE injectorProcess;
        const char* helperPath;
        const char* hookDllPath;
    };

    Injetor injetor;

    bool is64bitWindows(void)
    {
#ifdef _WIN64
        return true;
#else
        BOOL x86 = false;
        bool success = !!IsWow64Process(GetCurrentProcess(), &x86);
        return success && !!x86;
#endif
    }

    static inline bool has_utf8_bom(const char* in_char)
    {
        uint8_t* in = (uint8_t*)in_char;
        return (in && in[0] == 0xef && in[1] == 0xbb && in[2] == 0xbf);
    }

    size_t utf8ToWchar(const char* in, size_t insize, wchar_t* out,
        size_t outsize, int flags)
    {
        int i_insize = (int)insize;
        int ret;

        if (i_insize == 0)
            i_insize = (int)strlen(in);

        /* prevent bom from being used in the string */
        if (has_utf8_bom(in)) {
            if (i_insize >= 3) {
                in += 3;
                i_insize -= 3;
            }
        }

        ret = MultiByteToWideChar(CP_UTF8, 0, in, i_insize, out, (int)outsize);

        UNUSED_PARAMETER(flags);
        return (ret > 0) ? (size_t)ret : 0;
    }

    size_t osUtf8ToWcs(const char* str, size_t len, wchar_t* dst,
        size_t dst_size)
    {
        size_t in_len;
        size_t out_len;

        if (!str)
            return 0;

        in_len = len ? len : strlen(str);
        out_len = dst ? (dst_size - 1) : utf8ToWchar(str, in_len, NULL, 0, 0);

        if (dst) {
            if (!dst_size)
                return 0;

            if (out_len)
                out_len =
                utf8ToWchar(str, in_len, dst, out_len + 1, 0);

            dst[out_len] = 0;
        }

        return out_len;
    }

    size_t osUtf8UoWcsPtr(const char* str, size_t len, wchar_t** pstr)
    {
        if (str) {
            size_t out_len = osUtf8ToWcs(str, len, NULL, 0);

            *pstr = (wchar_t*)malloc((out_len + 1) * sizeof(wchar_t));
            return osUtf8ToWcs(str, len, *pstr, out_len + 1);
        }
        else {
            *pstr = NULL;
            return 0;
        }
    }

    bool createInjectProcess(const char* inject_path,
        const char* hook_dll,UINT thread_id,UINT process_id)
    {
        wchar_t* command_line_w = (wchar_t*)malloc(4096 * sizeof(wchar_t));
        wchar_t* inject_path_w;
        wchar_t* hook_dll_w;
        bool anti_cheat = true;
        PROCESS_INFORMATION pi = { 0 };
        STARTUPINFOW si = { 0 };
        bool success = false;

        osUtf8UoWcsPtr(inject_path, 0, &inject_path_w);
        osUtf8UoWcsPtr(hook_dll, 0, &hook_dll_w);

        si.cb = sizeof(si);

        swprintf(command_line_w, 4096, L"\"%s\" \"%s\" %lu %lu", inject_path_w,
            hook_dll_w, (unsigned long)anti_cheat,
            anti_cheat ? thread_id : process_id);

        success = !!CreateProcessW(inject_path_w, command_line_w, NULL, NULL,
            false, CREATE_NO_WINDOW, NULL, NULL, &si,
            &pi);
        if (success) {
            CloseHandle(pi.hThread);
            injetor.injectorProcess = pi.hProcess;
        }
        else {
            DbgOut("Failed to create inject helper process: %lu",
                GetLastError());
        }

        free(command_line_w);
        free(inject_path_w);
        free(hook_dll_w);
        return success;
    }

    void preInjet(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();

        // 检查传入的参数数量。
        if (args.Length() < 1)
        {
            // 抛出传回 JavaScript 的错误
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong number of arguments")
                    .ToLocalChecked()));
            return;
        }

        // 检查参数类型
        if (!args[0]->IsString())
        {
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong arguments")
                    .ToLocalChecked()));
            return;
        }

        // 执行操作
        std::string pathStd = ToCString(args[0].As<String>());
        hlog(pathStd.c_str());
        
        
        //判断当前是否64位程序在工作
        if (is64bitWindows())
        {
            /* code */
            char* result = (char*)malloc(strlen(pathStd.c_str()) + strlen("..\data\obs-plugins\win-capture\inject-helper64.exe") + 1);
			if (result != NULL) {
				strcpy(result, pathStd.c_str());
				strcat(result, "\data\obs-plugins\win-capture\inject-helper64.exe");
				injetor.hookDllPath = result;
			}
            else {
                DbgOut("");
            }
        }
        else {
            /* code */
            char* result = (char*)malloc(strlen(pathStd.c_str()) + strlen("..\data\obs-plugins\win-capture\inject-helper64.exe") + 1);
            if (result != NULL) {
                strcpy(result, pathStd.c_str());
                strcat(result, "\data\obs-plugins\win-capture\inject-helper32.exe");
                injetor.hookDllPath = result;
            }
            else {
                DbgOut("");
            }
        }
        
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    void startInjet(const FunctionCallbackInfo<Value>& args)
    {
        Isolate* isolate = args.GetIsolate();
        // 执行操作
        createInjectProcess(injetor.helperPath, "", 0, 0);

        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    void Init(Local<Object> exports)
    {
        NODE_SET_METHOD(exports, "preInjet", preInjet);
        NODE_SET_METHOD(exports, "startInjet", startInjet);
    }

    NODE_MODULE(dx_helper, Init)

} // namespace demo