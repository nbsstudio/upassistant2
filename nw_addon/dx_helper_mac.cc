#include <node.h>
#include <iostream>

namespace main
{

    using v8::Exception;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Number;
    using v8::Object;
    using v8::String;
    using v8::Value;


    void nothing(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();
        
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    

    void Init(Local<Object> exports)
    {
        NODE_SET_METHOD(exports, "preInjet", nothing);
    }

    NODE_MODULE(dx_helper, Init)

} // namespace demo