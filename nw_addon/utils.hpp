#pragma once

#include <windows.h>
#include <node.h>

#define DEBUG_OUTPUT

#ifdef DEBUG_OUTPUT
#define DbgOut(x) OutputDebugStringA(x)
#else
#define DbgOut(x)
#endif

using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

const char *ToCString(Local<String> strValue)
{
    v8::String::Utf8Value value(strValue);
    return *value ? *value : "<string conversion failed>";
}

static inline void hlogv(const char* format, va_list args)
{
	char message[1024] = "";
	int num = _vsprintf_p(message, 1024, format, args);
	if (num > 0) {
		DbgOut("[UPAssistant2] ");
		DbgOut(message);
		DbgOut("\n");
	}
}

void hlog(const char* format, ...)
{
	va_list args;

	va_start(args, format);
	hlogv(format, args);
	va_end(args);
}