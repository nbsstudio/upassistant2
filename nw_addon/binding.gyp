# use command "nw-gyp rebuild --target=0.63.1 --arch=x64" to build
{
  "targets": [
    {
      "target_name": "main_helper",
      "sources": [ "utils.hpp" ],
      "conditions":[
        ['OS=="win"',{"sources":["main_helper.cc"]}],
        ['OS=="mac"',{"sources":["main_helper_mac.cc"]}]
      ]
    },
    {
      "target_name": "dx_helper",
      "sources": [  ],
      "conditions":[
        ['OS=="win"',{"sources":["dx_helper.cc"]}],
        ['OS=="mac"',{"sources":["dx_helper_mac.cc"]}]
      ]
    }
  ]
}