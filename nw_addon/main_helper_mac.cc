#include <node.h>
#include <iostream>
namespace main
{

    using v8::Exception;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Number;
    using v8::Object;
    using v8::String;
    using v8::Value;

    // 这是 "add" 方法的实现
    // 输入参数使用
    // const FunctionCallbackInfo<Value>& args 结构传入
    void mouseFree(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    void updateProcessorIcon(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    void Init(Local<Object> exports)
    {
        NODE_SET_METHOD(exports, "mouseFree", mouseFree);
        NODE_SET_METHOD(exports, "updateProcessorIcon", updateProcessorIcon);
    }

    NODE_MODULE(main_helper, Init)

} // namespace demo