#include <node.h>
#include <iostream>
#include <tchar.h>
#include <Windows.h>
#include <WinUser.h>

namespace main
{

    using v8::Exception;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Number;
    using v8::Object;
    using v8::String;
    using v8::Value;

    // 这是 "mouseFree" 方法的实现
    void mouseFree(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();

        // 检查传入的参数数量。
        if (args.Length() < 1)
        {
            // 抛出传回 JavaScript 的错误
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong number of arguments")
                    .ToLocalChecked()));
            return;
        }

        // 检查参数类型
        if (!args[0]->IsNumber())
        {
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong arguments")
                    .ToLocalChecked()));
            return;
        }

        // 执行操作
        double value =
            args[0].As<Number>()->Value();
        Local<Number> num = Number::New(isolate, value);

        HWND ufHwnd = FindWindowW(NULL, L"nacgame-osd-mouse-free-window");
        if (value == 1)
        {
            //设置穿透
            SetWindowLongW(ufHwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED | WS_EX_TOOLWINDOW);
        }
        else
        {
            //取消穿透
            SetWindowLongW(ufHwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_TOOLWINDOW);
        }
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(num);
    }

    void updateProcessorIcon(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();

        // 检查传入的参数数量。
        if (args.Length() < 1)
        {
            // 抛出传回 JavaScript 的错误
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong number of arguments")
                    .ToLocalChecked()));
            return;
        }

        // 检查参数类型
        if (!args[0]->IsString())
        {
            isolate->ThrowException(Exception::TypeError(
                String::NewFromUtf8(isolate,
                                    "Wrong arguments")
                    .ToLocalChecked()));
            return;
        }

        // 执行操作
        // Node String --> String --> c_str --> lpcwstr
        Local<String> m = Local<String>::Cast(args[0]);
        String::Utf8Value titleNodeStr(isolate,m);
        std::string titleStr = std::string(*titleNodeStr);
        int bufSize = MultiByteToWideChar(CP_ACP, 0, titleStr.c_str(), -1, NULL, 0);
        wchar_t* titleLPCWSTR = new wchar_t[bufSize];
        MultiByteToWideChar(CP_ACP, 0, titleStr.c_str(), -1, titleLPCWSTR, bufSize);

        HINSTANCE hInst = GetModuleHandle(NULL);
        
        HWND ufHwnd = FindWindowW(L"Intermediate D3D Window", titleLPCWSTR);
        HWND uvHwnd = FindWindowW(NULL, L"live-room-viewer");
        // 设置返回值
        // （使用传入的 FunctionCallbackInfo<Value>&）
        args.GetReturnValue().Set(0);
    }

    void canUseCSharpAddon(const FunctionCallbackInfo<Value> &args)
    {
        Isolate *isolate = args.GetIsolate();
        // 检查是否有C# 4.5
        LPCWSTR regeditStr = L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4";
        bool canUse = false;
        HKEY hKey;
        if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, regeditStr, 0, KEY_ALL_ACCESS, &hKey))
        {
            RegCloseKey(hKey);
            canUse = true;
        }
        // 设置返回值
        args.GetReturnValue().Set(canUse);
    }

    void Init(Local<Object> exports)
    {
        NODE_SET_METHOD(exports, "mouseFree", mouseFree);
        NODE_SET_METHOD(exports, "updateProcessorIcon", updateProcessorIcon);
        NODE_SET_METHOD(exports, "canUseCSharpAddon", canUseCSharpAddon);
    }

    NODE_MODULE(main_helper, Init)

} // namespace demo