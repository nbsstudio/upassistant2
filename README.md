# UP主助手VERSION2

## 开发
开发环境：Windows 7+、MacOS 10.13+、Node.js 12.0+、 Yarn任意版本  
使用框架：VUE 2.0、JQuery1.8、Webpack
首次下载开发包时执行：
```
npm install -g node-gyp
npm install -g yarn
```
然后在代码目录下运行：
```
yarn install
```
### 首次开发注意事项
1.为便于调试点歌系统，请下载ffmpeg.exe到源代码根目录。 
2.请把“开发前手动安装的文件”目录下的win_delay_load_hook.cc覆盖<npm-path>\node_modules\node-gyp\src\win_delay_load_hook.cc  
3.[windows]发布前请在“nw_addon”目录下执行此命令：nw-gyp rebuild --target=0.63.1 --arch=x64 && cp build\Release\main_helper.node ..\  
  [mac]发布前请在“nw_addon”目录下执行此命令：nw-gyp rebuild --target=0.63.1 --arch=x64 && cp build/Release/main_helper.node ../  
4.然后把nw_addon/build/Release/下的main_helper.node复制到代码的根目录  

每次调试时执行以下命令：
```
yarn run dev
//调试
yarn run start
```

## 发行
已由原作者在官方网站 http://www.nacgame.com/ 发行正式版本。

## 理想
做B站UP主圈中流行的直播辅助工具，希望能满足UP主在直播过程中的各种要求，方便UP主空出双手做更多事情，例如查看弹幕、点播音乐、观察数据、快捷命令等，同时也可以通过各种插件，扩展更多功能！

## 参与建设
作者希望与其他有识之士一起搞建设，组成社区，才可以一起把这个工具点亮并发光发热！请加我QQ群：708719914、81637550
![./U201.png](/nbsstudio/upassistant2/raw/master/UP201.png)