#include <iostream>
#include <tchar.h>
#include <Windows.h>
#include <WinUser.h>

#include "Upassistant.h"

void startUpassistant()
{
    //启动UP主助手并一直等到程序关闭
    
    STARTUPINFOW si;
    memset(&si, 0, sizeof(STARTUPINFOW));
    si.cb = sizeof(STARTUPINFOW);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_SHOW;
    PROCESS_INFORMATION pi;

    Upassistant *ups;
    ZeroMemory(&si, sizeof(si));
    if (CreateProcessW(L"upassistant.exe",NULL,NULL,NULL,FALSE,0,NULL,L".\\",&si,&pi))
    {
        WaitForSingleObject(pi.hProcess, INFINITE);
        //UP主助手关闭之后，本程序同时退出
        CloseHandle( pi.hProcess );
        delete ups;
        exit(0);
    }
    else
    {
        delete ups;
        exit(2);
    }
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, int)
{
    HANDLE hwnd = CreateMutexA(NULL, FALSE, "Upassistant");
    DWORD e = GetLastError();
    if (e == ERROR_ALREADY_EXISTS)
    {
        MessageBoxW(NULL, L"您已经运行了一个UP主助手的程序了！", L"错误", MB_OK);
    }
    else
    {
        int n = 0;
        //启动UP主助手
        startUpassistant();
    }
    return 0;
}