; 脚本由 Inno Setup 脚本向导 生成！
; 有关创建 Inno Setup 脚本文件的详细资料请查阅帮助文档！

#define MyAppName "UP主助手2"
#define Ver = "Beta2.0版"
;#define Ver = "公测版V6"
#define VersionPart1 "2"
#define VersionPart2 "0"
#define VersionPart3 "2"
#define VersionPart4 "2"
#define MyAppPublisher "豪豪BOSS"
#define MyAppURL "http://www.nacgame.com"
#define MyAppExeName "UPassistant.exe"

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
; (生成新的GUID，点击 工具|在IDE中生成GUID。)
AppId={{4224AA82-6D20-44CA-925D-F2AB71E4DDD2}
AppName={#MyAppName}-{#VersionPart1}.{#VersionPart2}.{#VersionPart3}.{#VersionPart4}(32与64位安装包)
AppVersion={#VersionPart1}.{#VersionPart2}.{#VersionPart3}.{#VersionPart4}-{#Ver}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DisableProgramGroupPage=yes
LicenseFile=.\许可文本.txt
OutputDir=.\安装包
OutputBaseFilename=豪豪UP主助手{#VersionPart1}.{#VersionPart2}.{#VersionPart3}.{#VersionPart4}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"
; Name: "chinesesimp"; MessagesFile: "compiler:Languages\Chinese.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked;

[Types]
Name: "x64"; Description: "64位版本（适合64位操作系统使用）"
Name: "full"; Description: "32位版本（适合32位操作系统使用）"

[Components]
Name: "program"; Description: "主程序"; Types: full; Flags: fixed
Name: "programx64"; Description: "64位主程序"; Types: x64; Flags: fixed

[Dirs]
Name: "{app}\bw"
Name: "{app}\cache"
Name: "{app}\locales"
Name: "{app}\swiftshader"

[Files]
;32位版本
Source: "..\dist\Upassistant-2.0.2.2-win-x86\Upassistant.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\notification_helper.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\nw.dll"; DestDir: "{app}";Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\nw_elf.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\d3dcompiler_47.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\ffmpeg.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\libGLESv2.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\libEGL.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x86\node.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
;Source: "require_lib\x32\libgcc_s_dw2-1.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
;Source: "require_lib\x32\libstdc++-6.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
;Source: "require_lib\x32\libwinpthread-1.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "require_lib\x32\*.node"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
;64位版本
Source: "..\dist\Upassistant-2.0.2.2-win-x64\UPassistant.exe"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\notification_helper.exe"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\nw.dll"; DestDir: "{app}";Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\nw_elf.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\d3dcompiler_47.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\ffmpeg.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\libGLESv2.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\libEGL.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\node.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
;Source: "require_lib\x64\libgcc_s_seh-1.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
;Source: "require_lib\x64\libstdc++-6.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
;Source: "require_lib\x64\libwinpthread-1.dll"; DestDir: "{app}"; Components: programx64; Flags: ignoreversion
Source: "require_lib\x64\*.node"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
;公共
Source: "..\dist\Upassistant-2.0.2.2-win-x64\index.html"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\ffmpeg.exe"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\v8_context_snapshot.bin"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\package.json"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\resources.pak"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\nw_200_percent.pak"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\icudtl.dat"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\credits.html"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\default\ccnf.json"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion onlyifdoesntexist
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\zh-CN.pak"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\zh-CN.pak.info"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\en-US.pak"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\en-US.pak.info"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\zh-TW.pak"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\locales\zh-TW.pak.info"; DestDir: "{app}\locales"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\*.html"; DestDir: "{app}\bw"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\*.js"; DestDir: "{app}\bw"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\*.wasm"; DestDir: "{app}\bw"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\*.png"; DestDir: "{app}\bw"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\*.jpg"; DestDir: "{app}\bw"; Components: program programx64; Flags: ignoreversion
Source: "..\dist\Upassistant-2.0.2.2-win-x64\bw\theme\default\*"; DestDir: "{app}\bw\theme\default"; Components: program programx64; Flags: ignoreversion
Source: "..\logo2.png"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
;Launcher
Source: "..\LauncherC\build\windows\x86\release\Launcher.exe"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion
;Source: "..\LauncherC\build\*.dll"; DestDir: "{app}"; Components: program programx64; Flags: ignoreversion skipifsourcedoesntexist

; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion”


[Icons]
Name: "{commonprograms}\UP主助手2"; Filename: "{app}\Launcher.exe"
Name: "{commondesktop}\UP主助手2"; Filename: "{app}\Launcher.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\Launcher.exe"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
;Filename: "{app}\TTS修复.exe";Description: "修复TTS语音朗读弹幕功能"; Components:program programx64;Flags: nowait postinstall


