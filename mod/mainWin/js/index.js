/**
 * 主界面
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';

import '../less/index.less';
import $ from 'jquery';
import Bilibili from '../../../core/js/bilibili';
// import Bilibili from '../../../core/js/shanmite';
import Monitor from '../../../core/js/bilibili_monitor';
import Frame from '../../../core/js/frame';
import AddonAPI, { api } from '../../../core/js/addonAPI';
import CoreAPI from '../../../core/js/coreAPI';
import mvvm from 'vue';
import OBSPageServer from '../../OBSPage/js/server';
import layer from 'layer-src';
import {Solar} from 'lunar-javascript';
import mainGui from 'nwGui'; //NW框架的实体 
import addonAPI from '../../../core/js/addonAPI';
// import { frame } from 'ws/lib/sender';
import dxHelper from 'dxHelper';
import moment from 'moment';

let OSDWindow = null;
let ipWindow = null;
const TIPS1 = "请输入房间号";
let app = new mvvm({
  el: "#app",
  data: {
    /* 配置部分 */
    config:{
      showSayIco: false,
      showUserLevel: false,
      showUserAvatar: false,
      showTray:false,
      maxGiftRC:200,
      obsPath:"",
      enableSTS:false,
      includeSender:true, //朗读弹幕时是否包含发送人
      upgradePlanSet:{}
    },
    /* 状态部分 */
    showRoomidInput: false,
    showOnBusy:false,
    sending:false,
    loading:false,
    reciving:false,
    roomid: TIPS1,
    cody:[],
    wc:[],
    gf:[],
    compTags:[],
    ticid:null,
    /* 视图控制部分 */
    view:{
      menu:true,
      settingPage:false,
      stsPage:false,
      vfpsPage:false,
      votePage:false,
      currentPageSOHEvent: null
    },
    /**设置页面用 */
    tmpConfig:{},//配置主窗口的内容。某系配置项会跟OSD的配置项联动，所以要注意下面的代码编写别搞混了
    osdTmpConfig:{},//配置OSD的内容。
    /**插件部分 */
    plugin:{
      items:[{title:"安装插件",icon:"65e02e2df9621810ce6c1ad3a7e0bda0.png",tips:"",version:"install",action:"this.installPlugin();"}],
      cols:3, //用于主页遍历，插件图标有多少列（实时计算，(区域宽度 ➗ 图标宽度)取地板）
      rows:1 //用于主页遍历，插件图标有多少行（实时计算，总数量 ➗ 列）
    }
  },
  created:function(){
    let _this = this;
    //console.log(mainGui.App.argv);//['-port', '8080']
    /*打开弹幕窗口*/ 
    nw.Window.open('bw/OSD.html', { frame: false,icon:'logo2.png', show_in_taskbar: false, transparent: true, width: 500, height: 300, title: '' }, function (win) {
      win.on('loaded', function () {
        OSDWindow = win;
        win.setAlwaysOnTop(true);
        _this.componentLoaded('osd');
      });
    });
    fixLmiddleHeight();
    //初始化组件
    Bilibili.init(Monitor);
    CoreAPI.init();
    AddonAPI.init(window.API);
    CoreAPI.createDBInstance(function () {
      //2020-08-23新增
      CoreAPI.createTableIfNotExists();
      //2022-04-09新增，检查插件
      _this.checkPlugin();
      nw.global.CoreAPI = CoreAPI;
    });
    //20210614 added,启动OBS页面
    let server = new OBSPageServer();
    server.startup();
    //中国特色慰问
    this.happy();
  },
  methods: {
    happy: function () {
      var date = new Date();
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();
      let solar = Solar.fromYmd(year, month, day);
      // let solar2 = Solar.fromYmd(2022, 2, 1);
      // console.log(solar2.getLunar().toFullString());
      let lunarStr = solar.getLunar().toFullString();
      if(lunarStr.indexOf("(除夕)") > -1 || lunarStr.indexOf("(春节)") > -1){
        Frame.alert('新春快乐，今天的直播一定非常好看！');
      }
    },
    ridact:function(enterKeyPress){
      if(enterKeyPress){
        this.showRoomidInput = false;
      }else{
        this.showRoomidInput = !this.showRoomidInput;
      }
      if(this.showRoomidInput){
        if(this.roomid == TIPS1){
          this.roomid = "";
        }
        //让输入框获取焦点
        $('.cody-form input').focus();
      }else{
        if(this.roomid == ""){
          this.roomid = TIPS1
        }
      }
    },
    start: function () {
      if (this.roomid == "" || this.roomid === TIPS1 || this.roomid.length > 11) {
        Frame.alert('请输入房间代码');
        return;
      }
      this.sending = true;
      if (!this.reciving) {
        let _this = this;
        //连接直播房间
        Bilibili.openOne(this.roomid, false,this.recivingGift,this.recivingTitanGiftEvent,this.recivingDkEvent,this.recivingWellcomeEvent);
        this.ticid = setInterval(() => {
          _this.refresh();
        }, 1000);
        $('.entry').addClass('live-stop');
        $('.entry').removeClass('entry');
      }else{
        //断开连接
        Bilibili.shutdown();
        CoreAPI.flushRec(true);
        this.reciving = false;
        this.sending = false;
        clearInterval(this.ticid);
        $('.live-stop').addClass('entry');
        $('.live-stop').removeClass('live-stop');
      }
    },
    refresh:function(){
      if(!this.reciving){
        //仍在连接时
        if(Bilibili.getIsConnTimeOut()){
          this.sending = false;
          layer.alert('无法连接到直播间！可能有以下原因：<br>1.房间号输错了；<br>2.无法连接到服务器。', {
            skin: 'layui-layer-lan'
            ,closeBtn: 0
            ,anim: 4 //动画类型
          });
          clearInterval(this.ticid);
          $('.live-stop').addClass('entry');
          $('.live-stop').removeClass('live-stop');
        }
        let info = Bilibili.getMyInfo();
        if(info){
          this.reciving = true;
          this.sending = false;
          this.updateUserInfo(info);
        }
      }else{
        //正在接收弹幕时
        // let res = Bilibili.getWellcomeMsg();
        // if (res != undefined && res) {
        //   let cp = Frame.compareWellcomMsg(this.wc,res);
        //   if(cp && cp.length > 0){
        //     for (const i in cp) {
        //       const element = cp[i];
        //       // nw.global.OSDRefresh(element, 'wc');
        //       CoreAPI.dispatch(element,'wc',this.config);
        //     }
        //   }
        //   this.wc = res;
        // }
      }
    },
    updateUserInfo:function(info){
      $('.user-p .avatar').css('background-image','url('+info.avatarUrl+')');
      this.config.userInfo = info;
    },
    isReciving:function(){
      //TODO 有自动纠错机制
      return this.reciving;
    },
    /**
     * 核心组件收到弹幕消息时
     * @param {*} dmk 弹幕对象
     */
    recivingDkEvent:function(dmk){
      if(!checkReptDmk(this.cody,dmk)){
        // nw.global.OSDRefresh(dmk, 'dm');
        CoreAPI.dispatch(dmk,'dm',this.config);
        this.cody.push(dmk);
      }
    },
    recivingGift:function(dmk){
      //收到小礼物事件
      this.gf.push(dmk);
      // nw.global.OSDRefresh(dmk,'gf');
      CoreAPI.dispatch(dmk,'gf',this.config);
      if(this.gf.length > this.config.maxGiftRC){
        this.gf.shift();
      }
    },
    recivingTitanGiftEvent:function(dmk){
      //收到大礼物事件
      this.gf.push(dmk);
      // nw.global.OSDRefresh(dmk,'tgf');
      CoreAPI.dispatch(dmk,'tgf',this.config);
      if(this.gf.length > this.config.maxGiftRC){
        this.gf.shift();
      }
    },
    recivingWellcomeEvent:function(dmk){
      if(!checkReptDmk(this.cody,dmk)){
        CoreAPI.dispatch(dmk,'wellcome',this.config);
        this.cody.push(dmk);
      }
    },
    componentLoaded:function(component){
      this.compTags.push(component);
      if(this.compTags.length == 1){ /***注意：以后组件/窗口的新增和删除都要改这个判断，避免BUG */
        //加载完组件之后读取组件的设置项
        Frame.readConfig(this.allConfigLoaded);
      }
    },
    allConfigLoaded:function(data){
      if(data.ok){
        this.config = data.mainConfig;
        if(data.roomid && data.roomid != TIPS1){
          this.roomid = data.roomid;
        }
        //检查排错
        Frame.checkConfigureFileCopy(data,this.allConfigLoaded);
        if(data.mainConfig.userInfo){
          this.updateUserInfo(data.mainConfig.userInfo);
        }
        //读取窗口区域
        nw.Window.get().moveTo(data.mainConfig.x,data.mainConfig.y);
        //读取窗口大小
        nw.Window.get().resizeTo(data.mainConfig.width,data.mainConfig.height);
        //托盘图标
        if(data.mainConfig.showTray == undefined || data.mainConfig.showTray === true){
          this.config.showTray = true;
          nw.global.initTrayIcon();
        }
        //设置默认的朗读规则
        if(data.mainConfig.includeSender == undefined){
          this.config.includeSender = true;
        }
        if(data.mainConfig.speakPerson == undefined){
          this.config.speakPerson = "4";
        }
        //2022-02-26新增，默认强制使用鼠标穿透
        data.osdConfig.mouseFree = true;
        nw.global.OSDSetConfig(data.osdConfig);
        nw.global.VFPSSetConfig(data.vfpsConfig);
        
      }else{
        _this = this;
        //不存在配置文件，存一份新的
        Frame.saveAllConfig({
          mainConfig:_this.config,
          osdConfig:nw.global.OSDGetConfig(),
          vfpsConfig:nw.global.VFPSGetConfig(),
          includeSender:true,
          isFirstRun:true
        });
      }
    },
    saveAllConfig:function(){
      let configObject = {
        mainConfig:this.config,
        osdConfig:nw.global.OSDGetConfig(),
        vfpsConfig:nw.global.VFPSGetConfig(),
        roomid: this.roomid,
        ok:true,
        isFirstRun:false
      };
      Frame.saveAllConfig(configObject);
      //如果有正确输入房间号的，保存配置的同时保存一份备份，以应对下次启动读取不通过的时候可以自动恢复
      if(this.roomid !== undefined && this.roomid !== '' && this.roomid != TIPS1 && this.roomid.length > 2){
        Frame.saveAllConfigBackup(configObject);
      }
    },
    saveRecordData:function(){
      if(this.config.enableSTS){
        CoreAPI.flushRec();
      }
    },
    sohSettingPage:function(){ //开关设置页面
      this.tmpConfig = Frame.copy(this.config);
      this.osdTmpConfig = Frame.copy(nw.global.OSDGetConfig());
      this.view.menu = !this.view.menu;
      this.view.settingPage = !this.view.settingPage;
      this.tmpConfig.speakQty = CoreAPI.getSpeakQty();
      this.view.currentPageSOHEvent = this.sohSettingPage; //把事件指针缓存起来给通用“返回按钮”触发
    },
    sohStsPage:function(){ //开关统计页面
      if(!this.config.enableSTS){
        this.sohSettingPage();
        return;
      }
      this.view.menu = !this.view.menu;
      this.view.stsPage = !this.view.stsPage;
      this.view.currentPageSOHEvent = this.sohStsPage; //把事件指针缓存起来给通用“返回按钮”触发
      parent.window.stsRefresh();
    },
    sohVotePage:function(){ //开关点播台页面
      this.view.menu = !this.view.menu;
      this.view.votePage = !this.view.votePage;
      this.view.currentPageSOHEvent = this.sohVotePage; //把事件指针缓存起来给通用“返回按钮”触发
      parent.window.voteRefresh();
    },
    sohVfpsPage:function(){ //开关点播台页面
      this.view.menu = !this.view.menu;
      this.view.vfpsPage = !this.view.vfpsPage;
      this.view.currentPageSOHEvent = this.sohVfpsPage; //把事件指针缓存起来给通用“返回按钮”触发
      parent.window.vfpsRefresh();
    },
    backToMenu:function(){
      this.view.currentPageSOHEvent();
    },
    saveSettingPage:function(){
      if(this.tmpConfig.voiceVol > 9 || this.tmpConfig.voiceVol < 1){
        this.tmpConfig.voiceVol = 8
        Frame.alert('朗读音量只能是1到9');
      }
      this.config = Frame.copy(this.tmpConfig);
      //给OSD窗口相应的设置也同步一下
      this.syncOSDConfig();
      this.saveAllConfig();
      Frame.alert('保存成功！ヾ(◍°∇°◍)ﾉﾞ');
    },
    saveUGPSettingPage:function(tempConfig){
      try {
        this.config.upgradePlanSet = tempConfig;
        if(!tempConfig.enableCustomBaiduApi){
          CoreAPI.resetSpeakQty();
        }
        this.saveAllConfig();
        return true;  
      } catch (error) {
        return false;
      }
    },
    hasOBSFilePath:function(){
      return (this.config.obsPath && this.config.obsPath != '');
    },
    choiceExeFile:function(){
      $('#obs-file-select').click();
    },
    changeOBSFilePath:function(){
      let file = $('#obs-file-select').val();
      this.tmpConfig.obsPath = file;
    },
    syncOSDConfig:function(){
      let config = nw.global.OSDGetConfig();
      config.showUserLevel = this.config.showUserLevel;
      config.showGift = this.osdTmpConfig.showGift;
      config.onlyTitanGift = this.osdTmpConfig.onlyTitanGift;
      config.itemFade = this.osdTmpConfig.itemFade;
      config.fontSize = this.osdTmpConfig.fontSize;
      config.width = this.osdTmpConfig.width;
      config.height = this.osdTmpConfig.height;
      config.hideDMK = this.osdTmpConfig.hideDMK;
      nw.global.OSDSetConfig(config);
    },
    restoreOSDWindow:function(){
      let config = nw.global.OSDGetConfig();
      config.x = 0;
      config.y = 0;
      config.width = 500;
      config.height = 300;
      nw.global.OSDSetConfig(config);
    },
    startOBS:function(){
      if(this.hasOBSFilePath()){
        nw.Shell.openItem(this.config.obsPath);
      }else{
        Frame.alert('没有找到OBS程序的位置，请在设置中指定！');
        this.sohSettingPage();
      }
    },
    openLiveRoom:function(){
      if(this.roomid && this.roomid != ''){
        nw.Shell.openExternal('https://link.bilibili.com/p/center/index#/my-room/start-live');
      }else{
        Frame.alert('请先设定直播间号！');
      }
    },
    //检查插件，比如是否支持V1.x的插件，安装插件的顺序之类的问题
    checkPlugin:async function () {
      addonAPI.checkPlugin(this.pluginCheckedOK);
    },
    //插件检查完成事件
    pluginCheckedOK:function () {
      //加载一下插件列表
      let pluginData = addonAPI.loadList();
      if(pluginData.length > 0){
        let rows = pluginData[0].values;
        if(rows.length > 0){
          for (const key in rows) {
            if (Object.hasOwnProperty.call(rows, key)) {
              // console.log(rows[key]);
              let icon_path = (rows[key][2] != null && rows[key][2] != '')? rows[key][1]:'65e02e2df9621810ce6c1ad3a7e0bda0.png';
              let pluginInstance = this.loadPlugin(rows[key][0],rows[key][4]);
              // console.log(pluginInstance);
              this.plugin.items.push({title:rows[key][0],icon:icon_path,action:rows[key][10],instance:pluginInstance});
            }
          }
        }
      }
    },
    loadPlugin:function(title,version){
      return addonAPI.loadPlugin(title,version);
    },
    //安装插件
    installPlugin:function () {
      //弹出插件安装界面
      nw.Window.open('bw/pluginInstall.html',{icon:'logo2.png'},function(win){
        ipWindow = win;
        win.API = api;
      });
    },
    //根据大坐标获取插件对象
    getPlugin:function (col,row) {
      //vue索引转换
      let ri = row - 1;
      let ci = col - 1;
      //行索引 ✖ 列数 + 列索引
      let index = ri * this.plugin.cols + ci;
      // console.log(this.plugin.items[index]);
      return this.plugin.items[index];
    },
    runPluginAction:function (item) {
      if(item && item.action){
        //执行插件的图标命令
        eval(item.action);
      }else if(item && item.action == null){
        if(item.instance){
          item.instance.admin();
        }else{
          Frame.alert('该插件没有管理界面！');
        }
      }
    },
    moveableOSD:function(){
      //让OSD窗口闪一下
      nw.global.OSDFlashing();
    },
    changeVoiceVolume:function () {
      this.config.voiceVol = this.tmpConfig.voiceVol;
    },
    changeSpeakPerson:function(){
      //测试一下语音音色
      CoreAPI.baiduSpeak({content:"测试语音朗读！",userName:"助手"},'wellcome',this.tmpConfig);
    },
    upgradePlan:function(){
      let _this = this;
      this.showOnBusy = true;
      nw.Window.open('bw/upgradePlanWin.html', {focus:true,frame: true, show_in_taskbar: true,new_instance:false,kiosk:false,icon:'logo2.png'}, function (win) {
        win.on('loaded', function () {
          nw.global.upgradePlanSetConfig(_this.config.upgradePlanSet,_this.saveUGPSettingPage);
        });
        win.on('closed',function () {
          _this.showOnBusy = false;
          win = null;
        })
      });
    },
    debugDxHelper: function () {
      dxHelper.preInjet(this.config.obsPath);
    },
    /**
     * 对时间进行格式化
     * @param {*} dateStr 日期时间字符串
     * @param {*} pattern 格式化表达式
     * @returns 
     */
    dataFmt:function(dateStr,pattern='HH:mm:ss'){
      return moment(dateStr).format(pattern);
    },
    isRich:function(dkContent){
      return (dkContent.indexOf("<img") == 0 || dkContent.indexOf("<span") == 0);
    }
  }
});

// Listen to main window's close event
nw.Window.get().on('close', function () {
  //托盘图标的模式下，咨询用户是否要隐藏界面
  if(app.config.showTray){
    let winObj = this;
    layer.confirm('需要隐藏窗口到托盘图标么？（对着托盘图标按鼠标右键可重新激活）', {
      skin: 'layui-layer-lan',
      btn: ['隐藏','退出'] //按钮
    }, function(){
      layer.msg('隐藏中', {icon: 1});
      nw.Window.get().hide();
    }, function(){
      doClose(winObj);
    });
    return;
  }
  doClose(this);
});

function doClose(windowObject) {
  app.config.height = $(window).height();
  app.config.width = $(window).width();
  app.config.x = windowObject.x;
  app.config.y = windowObject.y;
  app.saveAllConfig();
  app.saveRecordData();
  if (Bilibili.running) {
    Bilibili.shutdown();
  }

  OSDWindow.close(true);
  windowObject.close(true);
  if(ipWindow != undefined && ipWindow != null){
    ipWindow.close(true);
  }
}

nw.Window.get().on('resize', function (width, height) {
  //修正部分控件大小
  fixLmiddleHeight();
});

function fixLmiddleHeight(){
  $('.lmiddle').css('height',($(window).height() - 72)+'px');
  $('#ctrn-vm').css('height',($(window).height() - $('.rtop').height() - $('#addon-vm').height())+"px");
  // $('.panel-body').css('height',$('.red-panel').height()-30);
}

function checkReptDmk(cody,dmk) {
  for (const i in cody) {
    if (Object.hasOwnProperty.call(cody, i)) {
      const element = cody[i];
      if(element.content === dmk.content){
        return true;
      }
    }
  }
  return false;
}