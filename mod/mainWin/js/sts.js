/**
 * 统计界面
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';

import '../less/index.less';
import '../less/sts.less';
import Vue from 'vue';

// let api = parent.window.API;
let api = nw.global.CoreAPI;
let app = new Vue({
    el: "#app",
    data: {
        sto: {
            dmc: 0,
            gfc: 0,
            wcc: 0,
            hmc: 0,
            hmr: 0,
            dmr: 0,
        }
    },
    created: function () {
        // this.getStoData();
    },
    methods: {
        getStoData: function () {
            let data = api.getDB() ? api.getDB().exec("select count(*) dmc from dmr where receive_time >= date('now','start of day')") : [];
            if (data.length > 0) {
                this.sto.dmc = data[0].values[0][0];
            }
            data = api.getDB() ? api.getDB().exec("select count(*) gfc from gfr where receive_time >= date('now','start of day')") : [];
            if (data.length > 0) {
                this.sto.gfc = data[0].values[0][0];
            }
            data = api.getDB() ? api.getDB().exec("select count(*) wcc from wcr where receive_time >= date('now','start of day')") : [];
            if (data.length > 0) {
                this.sto.wcc = data[0].values[0][0];
            }
            data = api.getDB() ? api.getDB().exec("select count(*) from (select count(*) hmc from dmr where receive_time >= date('now','start of day') group by user_name)") : [];
            if (data.length > 0) {
                this.sto.hmc = data[0].values[0][0];
            }
            if(this.sto.hmc == 0 || this.sto.dmc == 0){
                this.sto.hmr = 0;
                this.sto.dmr = 0;
            }else{
                this.sto.hmr = (this.sto.gfc / this.sto.hmc) * 100;
                this.sto.dmr = (this.sto.dmc / this.sto.hmc) * 100;
            }
        },
        clean:function(){
            if(confirm("即将清空所有数据，继续操作吗？")){
                api.getDB().exec("delete from dmr");
                api.getDB().exec("delete from gfr");
                api.getDB().exec("delete from wcr");
                this.getStoData();
            }
        }
    }
});
parent.window.stsRefresh = function(){ app.getStoData(); } ;