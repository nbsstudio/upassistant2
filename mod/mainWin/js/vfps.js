/**
 * 点播界面
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';

import '../less/index.less';
import '../less/vfps.less';
import Vue from 'vue';
import $ from 'jquery';
// import jquery from 'jquery';
import frame from '../../../core/js/frame';
import music from '../../../core/js/music';
import layer from 'layer-src';
// import Player from 'player'; //20210613 added,C++版mp3播放器
import Router from '../../../core/js/router'
import md5 from 'md5';


// let api = parent.window.API;
let api = nw.global.CoreAPI;
// let audioPlay = document.createElement("AUDIO");
let audioPlay = document.createElement("VIDEO");
const NONE_PLAYING = -1;

let app = new Vue({
    el: "#app",
    data: {
        config:{
            volume:100,
            disableAutoPlay:false,
            localMusicDir:[],
        },
        keyword: '',
        showSearchBar: false,
        myList: [],
        currentList: [],
        searchResult: [],
        currentPlayingId: NONE_PLAYING,
        currentPlayingContent:'无',        
        nextContent:'',
        status:'paused',
        obsServerRouter:null
    },
    created: function () {
        let _this = this;
        this.getData();
        this.eventInit();
        audioPlay.autoplay = true;
        audioPlay.volume = this.config.volume / 100;
        audioPlay.onerror = this.onPlayError;
        music.musicService(frame,this.serviceErrorEvent); //音乐服务
        frame.newSlider('slider','slider-value'); //创建音量拖动条
        nw.global.VFPSGetConfig = function(){
            return _this.config;
        }
        nw.global.VFPSSetConfig = function(conf){
            _this.setConfig(conf);
        }
        nw.global.VFPSApplyLocalMusicLib = function(libs){
            _this.config.localMusicDir = libs;
        }
        this.obsServerRouter = new Router().getMeFromNWStorge('obsServerRouter');
    },
    methods: {
        eventInit: function () {
            $(document).mousedown(function (e) {
                if (!($(".search-list-main").is(":hidden"))) {
                    if ($(e.target).parents(".search-list-main").length == 0) {
                        app.showSearchBar = false;
                    }
                } else {
                    return true;
                }
            });
            $(audioPlay).on('ended', function () {
                if (app.currentList.length > 0) {
                    app.play('continue');
                }else{
                    if(!app.config.disableAutoPlay){
                        app.autoPlay();
                    }
                }
            });
            $(audioPlay).on('canplay', function () {
                // console.log('canplay');
            });
        },
        setConfig:function(conf){
            if(conf && JSON.stringify(conf) != '{}'){
                this.config = conf;
                this.changeVolume();
            }
        },
        menualSearch: function () {
            if (this.keyword) {
                this.showSearchBar = true;
                music.searchMusic(this.keyword, function (res) {
                    if (res.code == 200) {
                        app.searchResult = res.data.data;
                    }
                });
            }
        },
        /**
         * 往列表增加一个音乐对象
         * @param {MusicooObject} 音乐对象 
         */
        addToList: function (obj) {
            obj.sig = md5(obj.name+obj.artist);
            obj.range = this.myList.length;
            this.myList.push(obj);
            this.addToDB(obj);
        },
        addToDB: function(obj){
            let res = api.getDB() ? api.getDB().run("insert into my_music (artist, name, music_id, range ) values (?,?,?,?)", [obj.artist, obj.name, obj.id, obj.range]):0;
            // console.log('music added '+res+' rows')
        },
        updateToDB: function(obj){
            let res = api.getDB() ? api.getDB().run("delete from my_music where 1=1", []):0;
            for (const key in this.myList) {
                if (this.myList.hasOwnProperty(key)) {
                    const element = this.myList[key];
                    this.addToDB(element);
                }
            }
            res = api.getDB() ? api.flushRec(false):0;
        },
        /**
         * 观众点歌
         * @param {string} 关键词 
         */
        voteToList: function (keyword) {
            //搜索音乐
            music.searchMusic(keyword, function (res) {
                if (res.code == 200) {
                    const list = res.data.data;
                    if (list && list.length > 0) {
                        if (!app.checkRept(list[0].id)) {
                            app.currentList.push(list[0]);
                            app.autoPlay(false);
                        }
                    }
                }
            });
        },
        /**
         * 自动开播
         * @param {boolean} 是否手动操作的？ 
         * @returns 
         */
        autoPlay: function (menual) {
            if(audioPlay && this.currentPlayingId != NONE_PLAYING && this.status != 'paused'){
                if(menual){
                    alert('已经开播了！');
                }
                return ;
            }
            if (this.myList.length > 0 || this.currentList.length > 0) {
                if (this.currentList.length == 0) {
                    for (const key in this.myList) {
                        if (this.myList.hasOwnProperty(key)) {
                            const element = this.myList[key];
                            //查重
                            if (!this.checkRept(element.id)) {
                                this.currentList.push(element);
                            }
                        }
                    }
                }
                if(!this.config.disableAutoPlay){
                    this.play('vfp');
                }
            }
        },
        checkRept: function (id) {
            if (this.currentPlayingId == id) {
                return true;
            }
            for (const j in this.currentList) {
                if (this.currentList.hasOwnProperty(j)) {
                    const h = this.currentList[j];
                    if (id == h.id) {
                        return true;
                    }
                }
            }
            return false;
        },
        play: function (flag) {
            //TODO 以后可以与启动器联动，调用Windows版启动器中的音频播放器，并实现选择声卡设备输出
            if(!flag){
                flag = 'normal';
            }
            /**
             * 当满足以下条件时，取当前列表第一项媒体进行下载和播放
             * 1.播放器播放完毕时(continue)
             * 2.切歌功能触发时(jump)
             * 3.强制切歌(force_jump)
             * 4.观众点歌并且播放器未播放媒体时
             * 5.播放器初始化后
             */
            if ( flag=='continue' || (flag == 'jump' && !audioPlay.paused) || flag == 'force_jump' || flag=='vfp' || (audioPlay.paused && audioPlay.src == "")) {
                //当前播放列表没有音乐了，只停止播放就可以了
                if(this.currentList.length == 0){
                    audioPlay.pause();
                    this.status = 'paused';
                    this.showNext();
                    this.currentPlayingContent = "无";
                    this.currentPlayingId = NONE_PLAYING;
                    return false;
                }
                const a = this.currentList.shift();
                if (a.url != undefined && "" != a.url) {
                    this.status = 'getting';
                    music.getMusic(a, function (data) {
                        app.status = 'processing';
                        if (data.code === 200) { //正常获取音乐
                            const M_PATH = data.data.replace('./cache/', '');
                            if (flag == 'jump') {
                                audioPlay.pause();
                                app.status = 'paused';
                            }
                            //使用音乐服务获取文件并播放
                            audioPlay.src = 'http://127.0.0.1:35/' + M_PATH;
                            // mp3Play = new Player('http://127.0.0.1:35/' + M_PATH);
                            app.currentPlayingId = a.id;
                            app.currentPlayingContent = a.artist + ' - ' + a.name;
                            app.showNext();
                            audioPlay.play(); //正常播放
                            app.status = 'playing';
                        }else if(data.code == 500){ //获取音乐失败
                            nw.global.OSDRefresh({
                                userName: '系统',
                                sendTimestamp: Date.parse(new Date()),
                                userLevel: '9999',
                                userRank: '1',
                                content: a.artist + '-'+a.name + data.message,
                                isGift: false,
                            }, 'dm');
                            app.play('jump'); //切歌
                            app.status = 'jumping';
                        }
                    })
                }
            } else if (audioPlay.paused) {
                try {
                    audioPlay.play();
                    this.status = 'playing';
                } catch (error) {
                    if(flag == 'jump'){
                        this.play('jump');
                    }
                }
                                
            }else if(flag == 'pause'){
                audioPlay.pause();
                this.status = 'paused';
            }
            this.obsServerRouter.emit('updateOBSPage',{area:"A",html:this.showOBSList()});//通知OBS刷新页面
            return true;
        },
        changeVolume:function(){
            audioPlay.volume = this.config.volume / 100;
        },
        /**
         * 显示下一首播放的内容
         * @description 在currentList第一首正在播放的时候调用，此时currentList的第一个元素是下一首音乐
         */
        showNext(){
            if(this.currentList.length > 0){
                this.nextContent = this.currentList[0].artist+' - '+this.currentList[0].name;
            }else{
                this.nextContent = '无 - 请点歌！'
            }
            this.obsServerRouter.emit('updateOBSPage',{area:"A",html:this.showOBSList()});//通知OBS刷新页面
        },
        /**
         * 获取OBS显示页面的内容
         */
        showOBSList(){
            let html = "";
            html += '<ul style="font-size:16px;list-style-type: none;">';
            html += '<li><span>当前正在播放：</span><span>'+this.currentPlayingContent+'</span>&nbsp;&nbsp;<span>即将播放：</span><span>'+this.nextContent+'</span></li>'
            html += '<li><span>点歌方法：发送弹幕&nbsp;点歌&nbsp;歌名（中间有空格哦！）</span></li>'
            html += '<li><span>列表：</span></li>'
            for (const key in this.currentList) {
                if (Object.hasOwnProperty.call(this.currentList, key)) {
                    const element = this.currentList[key];
                    html += '<li>'+element.name+'-'+element.artist+'</li>'
                }
            }
            html += '</ul>';
            return html;
        },
        getData: function () {
            try {
                let data = api.getDB() ? api.getDB().exec("select artist,name,music_id,range from my_music") : [];
            if (data.length > 0) {
                this.myList = [];
                for (const key in data[0].values) {
                    if (data[0].values.hasOwnProperty(key)) {
                        const element = data[0].values[key];
                        this.myList.push({artist:element[0],name:element[1],id:element[2],range:element[3],sig:md5(element[1]+element[0])});
                    }
                }
            }
            } catch (error) {
                layer.alert('音乐库数据无法读取！如需帮助请上官网或者加群。<a href="http://www.nacgame.com">http://www.nacgame.com</a>');
            }
            
        },
        deleteMyMusic:function(obj){
            if(obj && obj.sig){
                for (const key in this.myList) {
                    if (this.myList.hasOwnProperty(key)) {
                        const element = this.myList[key];
                        if(element.sig == obj.sig){
                            this.myList.splice(parseInt(key),1);
                            break;
                        }
                    }
                }
            }
            this.updateToDB();
        },
        /**
         * 音乐服务出错事件
         * @param {errObj} 错误信息对象 
         */
        serviceErrorEvent: function (errObj) {
            //这种格式的音频目前不支持播放
            nw.global.OSDRefresh({
                userName: '系统',
                sendTimestamp: Date.parse(new Date()),
                userLevel: 'UL 9999',
                userRank: '1',
                content: errObj.url + '无法播放，格式转换错误！',
                isGift: false,
            }, 'dm');
            //自动切换到下一首
            app.play('jump');
        },
        onPlayError:function (e) {
            console.log('播放失败，正在强制切歌');
            //强制切歌
            this.play('force_jump');
        },
        localMusicLib(){
            layer.open({
                type: 2,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '240px'], //宽高
                content: 'localMusicLib.html'
            });
        }
        
    }
});
parent.window.vfpsRefresh = function () { app.getData(); };
parent.window.vfpsAdd = function (keyword) { app.voteToList(keyword); };
nw.global.addMusicToMyListAndInsert = function(data) {app.addToList(data);};

