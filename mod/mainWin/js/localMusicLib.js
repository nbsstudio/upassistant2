import '../less/index.less';
import '../less/localMusicLib.less';
import Vue from 'vue';
import $ from 'jquery';
import layer from 'layer-src';
import music from '../../../core/js/music'; 
// let api = parent.parent.parent.window.API;
let api = nw.global.CoreAPI;

let app = new Vue({
    el: "#app",
    data:{
        config:{
            localMusicDir:[]
        }
    },
    created:function () {
        //从占播页面中获取设置
        let vfpsConfig = nw.global.VFPSGetConfig();
        if(vfpsConfig && vfpsConfig.localMusicDir){
            this.config.localMusicDir = vfpsConfig.localMusicDir;
        }
    },
    methods:{
        add:function () {
            $("#localMusicDirInput").click();
        },
        addLocalMusicDir:function(e) {
            for (const i in this.config.localMusicDir) {
                if (Object.hasOwnProperty.call(this.config.localMusicDir, i)) {
                    const element = this.config.localMusicDir[i];
                    if(element.path === e.path){
                        layer.msg("已添加了此音乐库！");
                        return;
                    }
                }
            }
            let path = e.target.value;
            this.config.localMusicDir.push({path:path,disable:false});
            $("#localMusicDirInput").val('');
        },
        /**
         * 应用设置项
         */
        apply:function () {
            nw.global.VFPSApplyLocalMusicLib(this.config.localMusicDir);
            music.scanLocalMusic(this.config.localMusicDir);
            layer.msg('已应用');
        },
        /**
         * 删除音乐库项目
         * @param {object} 项目 
         */
        delete:function(item){
            this.config.localMusicDir.forEach(function(object, index, arr) {
                if(object.path === item.path) {
                  arr.splice(index, 1);
                }
            });
        },
        /**
         * 把音乐库项目添加到本地自己的播放
         * @param {object} 项目 
         */
         transToMyList:function(item){
            try {
                if(music.isScanningLocalMusic()){
                    layer.msg("还没扫描完音乐文件，请稍后再试！");
                    return;
                }
                let data = api.getDB() ? api.getDB().exec("select artist,title,path,lib_path from local_music_lib where lib_path = '"+item.path+"'") : [];
                if(data.length > 0){
                    let localMusicValues = data[0].values;
                    if(localMusicValues.length > 0){
                        for (const key in localMusicValues) {
                            if (Object.hasOwnProperty.call(localMusicValues, key)) {
                                const element = localMusicValues[key];
                                // let res = api.getDB() ? api.getDB().run("insert into my_music (artist, name, music_id, range ) values (?,?,?,?)", element[0], obj.name, obj.id, obj.range]):0;
                                // console.log('music added '+res+' rows')
                                nw.global.addMusicToMyListAndInsert({artist:element[0],name:element[1],id:Number(new Date()), local: true, path: element[2],url:"file://"+element[2]});
                            }
                        }
                    }
                }
                
            } catch (error) {
                layer.msg("添加到我的播放列表失败，未知错误！");
            }
        }
    }
});