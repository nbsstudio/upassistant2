/**
 * 投票界面
 * @author 周毅豪
 * @license GPL v2.0
 */
 'use strict';

 import '../less/index.less';
 import '../less/vote.less';
 import Vue from 'vue';
 import layer from 'layer-src';
 import Router from '../../../core/js/router';

 let app = new Vue({
     el: "#app",
     data: {
         config:{
         },
         routerObject:null,
         running:false,
         mode:0, //投票机制，0是按票数，1是按时间范围(TODO)
         max:100,//UP主设定的最大票数
         objects:[], //投票对象
         isExtend:false
     },
     created: function () {
         let _this = this;
     },
     methods: {
         /**新建对象 */
        addObject:function () {
            if(this.objects.length < 256){
                let obj = {title:"点赞",index:this.objects.length + 1,count:0,progress:0,editting:false};
                this.objects.push(obj);
                layer.prompt({title: '输入投票对象的内容并确认！', formType: 2}, function(text, index){
                    layer.close(index);
                    obj.title = text;
                });
            }else{
                layer.alert("兄弟，这投票选项太多了，不能再增加了！",{icon:0});
            }
        },
        changeTitle:function (obj) {
            obj.editting = !obj.editting;
        },
        /**开始或关闭投票计算 */
        letsGo:function () {
            if(this.objects.length > 1){
                this.running = !this.running;
                //注册弹幕事件（用懒加载的原因是为了确保对象可用）
                let bilibiliCore = new Router().getMeFromNWStorge("bilibili");
                if(this.routerObject == null && bilibiliCore){
                    this.routerObject = bilibiliCore;
                    this.routerObject.regist('ordkEvent',this.com);
                }
            // //测试
            // let _this = this;
            // setInterval(() => {
            //     _this.com({content:"2"});
            // }, 1000);
            }else{
                layer.alert("兄弟，两个投票对象都没有，没发开始啊！",{icon:0});
            }
            
        },
        /**清除 */
        purge:function () {
            let _this = this;
            layer.confirm('确认删除所有对象吗？', {
                btn: ['不删','删吧'] //按钮
              }, function(){

              }, function(){
                _this.objects = [];
              });
        },
        /**消息来了 */
        com:function (data) { 
            if(data && data.content){
                let msg = data.content.trim();
                for (const key in this.objects) {
                    if (Object.hasOwnProperty.call(this.objects, key)) {
                        const obj = this.objects[key];
                        //内容或者序号对上了都计一票
                        if(obj.title == msg || obj.index == msg){
                            obj.count++;
                            if(this.mode === 0 && this.running){
                                this.calc(obj);
                            }
                        }
                    }
                }
            }
            console.log(data);
        },
        /**计算 */
        calc:function (obj) {
            let rate = (obj.count / this.max)*100;
            obj.progress = rate;
            if(obj.progress >= 100){
                this.running = false;
                layer.alert(obj.title + '获得了最高票数！', {
                    icon: 1
                });
                //TODO 播放声音提醒
            }
        },
        extendView:function(){
            this.isExtend = !this.isExtend;
        }
     }
 });
 parent.window.voteRefresh = function () { /* */};
 