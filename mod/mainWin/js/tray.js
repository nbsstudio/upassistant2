

/**
 * 初始化托盘图标（全局方法）
 */
function initTrayIcon() {
    // 创建一个托盘图标
    let tray = new nw.Tray({
        title: 'UP主助手2',
        tooltip: 'UP主助手2',
        icon: 'logo2.png'
    });

    // 新建一个菜单对象
    let menu = new nw.Menu();

    let itemChecked = true;

    // 创建菜单项
    let menuItems = [
        // {
        //     type: 'checkbox',
        //     label: 'Checkbox',
        //     checked: itemChecked,
        //     click: function () {
        //         itemChecked = !itemChecked;
        //         console.log(itemChecked);
        //     }
        // },
        {
            type: 'normal',
            label: '开发者工具',
            click: function () {
                alert('插件调试模式才可以启用！');
                // nw.Window.get().showDevTools();
            }
        },
        {
            type: 'normal',
            label: '显示窗口',
            click: function () {
                nw.Window.get().show();
            }
        },
        // {
        //     type: 'normal',
        //     label: 'Hide Window',
        //     click: function () {
        //         nw.Window.get().hide();
        //     }
        // },
        {
            type: 'separator'
        },
        {
            type: 'normal',
            label: '退出',
            click: function () {
                //目前的逻辑是先显示窗口，让用户再次从界面上选择“退出”，
                //TODO 之后的逻辑是，如果用户在设置或退出问题框中勾选了“不在询问，直接隐藏”的时候，这里就要让index.js接收强制关闭信号，以达到节省步骤的目的。
                nw.Window.get().show();
                nw.Window.get().close();
            }
        }
    ];

    // Append all menu items to the menu
    menuItems.forEach(function (item) {
        menu.append(new nw.MenuItem(item));
    });

    // Place the menu in the tray
    tray.menu = menu;
}

nw.global.initTrayIcon = initTrayIcon;