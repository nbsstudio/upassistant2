/**
 * 弹出层
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';
import '../less/index.less';
import $ from 'jquery'//请勿删除
import mvvm from 'vue';
import Frame from '../../../core/js/frame';
import Router from '../../../core/js/router';
import mainHelper from 'mainHelper';

let fullCache = false;
let cacheChecking = false;
let cache = [];
let app = new mvvm({
    el:"#app",
    data:{
        config:{
            showSayIco: false,
            showUserLevel: true,
            showGift:true,
            itemFade:true,
            onlyTitanGift:false,
            mouseFree:true,
            maxContain:200,
            x:0,
            y:0,
            width:500,
            height:300,
            fontSize:25,
            // maxCacheCount:3
        },
        routerObject:null,
        view:{
            hidden:false
        },
        cody: [],
        wc:[],
        showLy:false,
        showLyTime:8000,
        isSetting:false
    },
    created:function(){
        window.ondblclick = function(){
            return false;
        };
        let _this = this;
        this.init();
        nw.global.OSDGetConfig = function(){
            return _this.config;
        }
        nw.global.OSDSetConfig = function(conf){
            _this.setConfig(conf);
        }
        nw.global.OSDFlashing = function () {
            _this.flashing();
        }
        //设置为鼠标穿透
        mainHelper.mouseFree(1);
    },
    methods:{
        init:function () {
            let bilibiliCore = new Router().getMeFromNWStorge("bilibili");
            if(this.routerObject == null && bilibiliCore){
                this.routerObject = bilibiliCore;
                this.routerObject.regist('ordkEvent',this.recive);
                this.routerObject.regist('orgEvent',this.recive);
                this.routerObject.regist('ortgEvent',this.recive);
            }
            this.refresh({
                userName:'助手',
                sendTimestamp:Date.parse(new Date()),
                userLevel:'9999',
                userRank: '1',
                content:'本王弹幕准备就绪！',
                isGift:false,
            }, 'dm');
        },
        recive:function(danmaku) {
            if(danmaku.isGift && danmaku.titan){
                this.refresh(danmaku,'tgf')
            }else if(danmaku.isGift && !danmaku.titan){
                this.refresh(danmaku,'gf')
            }else{
                this.refresh(danmaku,'dm')
            }
        },
        refresh:function(danmaku, type){
            //净化实体(因为从主窗口传进来的对象引用会影响到本窗口对对象的操作)
            let newDanmaku = Frame.copy(danmaku);
            newDanmaku.show = true;
            //插入实体到OSD
            switch (type) {
                case 'dm':
                    cache.push(newDanmaku);
                    this.cody.push(newDanmaku);
                    break;
                case 'wc':
                    this.wc = [newDanmaku];
                    this.showLy = true;
                    let _this = this;
                    setTimeout(() => {
                        _this.showLy = false;
                    }, this.showLyTime);
                    break;
                case 'gf':
                    if(this.config.showGift && !this.config.onlyTitanGift){
                        cache.push(newDanmaku);
                        this.cody.push(newDanmaku);
                    }
                    break;
                case 'tgf':
                    //TODO 大礼物要有魔法
                    if (this.config.showGift) {
                        cache.push(newDanmaku);
                        this.cody.push(newDanmaku);
                    }
                    break;
                default:
                    break;
            }
            if(!cacheChecking){
                // let mcc = this.config.maxCacheCount;
                let mcc = 3;
                setTimeout(() => {
                    if(cache.length > mcc){
                        cache = [];
                        fullCache = true;
                    }else{
                        fullCache = false;
                    }
                    cacheChecking = false;
                }, 1000);
                cacheChecking = true;
            }
            this.scroll();
            if(this.cody.length > this.config.maxContain){
                this.cody.shift();
            }
            if(this.config.itemFade){
                this.fadeItems(newDanmaku);
            }
            if(this.config.itemFade && this.view.hidden){
                this.animateSOHWindow();
            }
        },
        scroll: function () {
            var mainContainer = $('.inside_content'),
                scrollToContainer = mainContainer.find('.son-panel:last');//滚动到<div id="thisMainPanel">中类名为son-panel的最后一个div处
            let ms = fullCache?5:500;
            //动画效果
            mainContainer.animate({
                scrollTop:  mainContainer.prop("scrollHeight")
            }, ms);
        },
        setConfig:function(conf){
            //自动修复上一版没有的配置，赋默认值
            if(!conf.maxContain){
                conf.maxContain = 200;
            }
            if(!conf.fontSize || conf.fontSize < 5){
                conf.fontSize = 25;
            }
            this.config = conf;
            // alert(conf.x);
            if(this.config.hasOwnProperty('hideDMK') && this.config.hideDMK != undefined && this.config.hideDMK){
                nw.Window.get().hide();
            }else{
                nw.Window.get().show();
            }
            nw.Window.get().moveTo(conf.x,conf.y);
            nw.Window.get().resizeTo(conf.width,conf.height);
        },
        animateSOHWindow:function(){
            //当弹幕完全空闲时，使整个窗口消失，当有弹幕来临时，动画显示
        },
        fadeItems:function(exclude){
            for (const i in this.cody) {
                if (this.cody.hasOwnProperty(i)) {
                    try {
                        if((parseInt(exclude.sendTimestamp) - parseInt(this.cody[i].sendTimestamp)) > 1000 ){
                            this.cody[i].show = false;
                        }    
                    } catch (error) {
                        
                    }
                }
            }
        },
        closeAndSavePosition:function () {
            this.config.mouseFree = true;
            mainHelper.mouseFree(1);
            if(this.config.hasOwnProperty('hideDMK') && this.config.hideDMK != undefined && this.config.hideDMK){
                //设置完了还原回去
                nw.Window.get().hide();
            }
            this.isSetting = false;
        },
        flashing:function () {
            if(this.config.hasOwnProperty('hideDMK') && this.config.hideDMK != undefined && this.config.hideDMK){
                //暂时显示一下窗口
                nw.Window.get().show();
            }
            this.config.mouseFree = false;
            this.isSetting = true;
            //先取消全局鼠标穿透
            mainHelper.mouseFree(0);
            //闪烁一下窗口
            $("#app").animate({opacity:"0"},150); //改变不透明度
            $("#app").animate({opacity:"1"},150);
            $("#app").animate({opacity:"0"},150);
            $("#app").animate({opacity:"1"},150);
        },
        checkDmkStatus:function(){
            //先判断是否正在设置窗口位置，不在设置状态的情况下，
            if(!this.isSetting){
                //如果本身是鼠标穿透但是依然触发了点击事件，重新设置穿透
                mainHelper.mouseFree(1);
            }
        }
    }
});
nw.Window.get().on('move', function (x,y) {
    app.config.x = x;
    app.config.y = y;
});
nw.Window.get().on('resize', function (w,h) {
    app.config.width = w;
    app.config.height = h;
});
nw.Window.get().on('maximize', function () {
    nw.Window.get().restore();
});