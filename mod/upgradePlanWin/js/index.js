/**
 * 免费功能升级界面一
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';

import '../less/index.less';
import mvvm from 'vue';
import layer from 'layer-src';
import frame from '../../../core/js/frame';

let app = new mvvm({
    el: "#app",
    data: {
        config: {baiduApiKey:'',baiduApiSecert:''}
    },
    created() {
        let _this = this;
        /**
         * 把免费功能升级的设置项放进本窗口
         * @param {*} conf 各个设置项
         * @param {*} saveUGPSettingPage 回调函数：保存本页面的设置项
         */
        nw.global.upgradePlanSetConfig = function (conf, saveUGPSettingPage) {
            
            if (conf != undefined && conf != null) {
                 _this.config = conf;
                 console.log(_this.config);
            }
            _this.saveUGPSettingPage = saveUGPSettingPage;
        }
    },
    methods: {
        validateBaiduApiParams(){
            if (this.config.baiduApiKey != '' || this.config.baiduApiSecert != '') {
                if(this.config.baiduApiKey.length < 24){
                    return false;
                }else if(this.config.baiduApiSecert.length < 32){
                    return false;
                }
                this.config.enableCustomBaiduApi = true; //验证成功的话自动设置“启用参数”
            }else{
                this.config.enableCustomBaiduApi = false;
            }
            return true;
        },
        /**
         * 触发保存本页面的设置项
         */
        save() {
            //逐个部分的参数进行校验
            if(!this.validateBaiduApiParams()){
                frame.alert('输入的百度语音API参数有误，请重新输入！');
                return;
            }
            //没有问题再保存
            if(this.saveUGPSettingPage(this.config)){
                frame.alert('保存成功！');
            }else{
                frame.alert('保存失败！');
            }
        }
    }
});