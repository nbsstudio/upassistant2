/**
 * OBS Studio 版本的浏览器对象的服务提供者
 * @author 周毅豪
 * 原理：
 * 初始化时，监听236端口-->注册一个消息路由。
 * OBS用浏览器对象加载这个页面时，Ajax发送心跳包-->如果心跳包有更新提示-->Ajax访问“获取内容接口”
 * 消息路由发来消息时，接收组件按标准对象发来的包-->缓存-->改状态
 * OBS访问“获取内容接口”时，取状态-->取内容包-->返回给OBS-->OBS渲染Ajax响应包里面的html到指定页面区域
 */
eval('var http = require("http")');
eval('var url = require("url")');
let Router = require('../../../core/js/router');

const HTML_HEAD = '<head><meta charset="utf-8"><title>OBS Show</title></head>';
//内部属性
let needUpdate = false;
let partContent = {area:'',html:''};
class Server {
    constructor() {
        this.httpInstance = null;
        //建立路由（内存共享）并注册内容更新事件
        this.router = new Router();
        this.router.regist('updateOBSPage', this.onUpdate)
        this.router.putMeToNWStorge('obsServerRouter');
    }

    /**
     * 启动服务
     */
    startup() {
        let _this = this;
        let httpInstance = http.createServer(function (request, response) {
            const INCOMING_URL = decodeURIComponent(request.url);
            response.writeHead(200, { 'Content-Type': 'text/html' });
            if (INCOMING_URL.indexOf('/getContents') > -1) {
                response.end(_this.makeContents());//写入动态HTML内容
            } else if (INCOMING_URL.indexOf('/needUpdate') > -1) {
                response.end(needUpdate ? "true" : "false");//告知浏览器是否需要更新内容
            } else {
                response.end(_this.makeBody());//写入HTML内容
            }
        });
        httpInstance.listen(236);
    }
    /**
     * 渲染主页内容
     * @returns {string} html
     */
    makeBody() {
        let html = '<html>';
        html += HTML_HEAD;
        html += '<body>';
        html += '<div id="A"></div>';//左区域
        html += '<div id="B"></div>';//上区域
        html += '<div id="C"></div>';//右区域
        html += '<div id="D"></div>';//下区域
        html += '<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"></script>';//加载jquery
        html += '<script type="text/javascript">';
        html += '$(document).ready(()=>{';
        html += '    console.log("研究啥呢！");';
        html += '    ask();';
        html += '});';
        html += 'var needUpdate = false;';
        html += 'function ask() {';
        html += '    $.get("./needUpdate", function (data) {';
        html += '        needUpdate = (data=="true");';
        html += '        if(needUpdate){';
        html += '            update();';
        html += '        }';
        html += '    })';
        html += '        .fail(function () {';
        html += '        })';
        html += '        .always(function () {';
        html += '            ask();';
        html += '        });';
        html += '}';
        html += 'function update() {';
        html += '    $.get("./getContents", function (data) {';
        html += '        console.log("更新来了！");';
        html += '        console.log(data);';
        html += '        var pak = JSON.parse(data);';
        html += '        $("#"+pak.area).html(pak.html);';
        html += '    })';
        html += '        .fail(function () {';
        html += '        })';
        html += '        .always(function () {';
        html += '        });';
        html += '}';
        html += '</script>';
        html += '</body>';
        return html;
    }
    /**
     * 动态的网页内容
     * @returns {string} html
     */
    makeContents() {
        //TODO: 这里可以受用户设置控制，用户可以选择不显示什么区域的内容
        let html = '';
        if(needUpdate){
            html = JSON.stringify(partContent);
            needUpdate = false;
        }
        return html;
    }
    /**
     * 关闭服务
     */
    shutdown() {
        if (this.httpInstance) {
            this.httpInstance.close(this.onClosed);
        }
    }
    /**
     * 关闭了服务(事件)
     */
    onClosed() {

    }
    /**
     * 更新部分内容(事件)
     * @param {*} 内容对象 
     */
    onUpdate(part) {
        needUpdate = true;
        partContent = part; //TODO: 这里用队列来存其实好一点的
    }
}

module.exports = Server;