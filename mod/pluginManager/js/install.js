/**
 * 插件安装界面
 * @author 周毅豪
 * @license GPL v2.0
 */
'use strict';

import '../less/install.less';
import Frame from '../../../core/js/frame';
import layer from 'layer-src';
import fs from 'fs';
import compressing from 'compressing';
import mvvm from 'vue';
import mainHelper from 'mainHelper';
const PLUGIN_DIR = 'up2_plugins';
let api = nw.global.CoreAPI;

let app = new mvvm({
    el: "#app",
    installProcess: 0,
    created() {
        this.getList();
    },
    methods: {
        getList: function () {
            //获取官网或者gitee上的插件列表
        },
        installVerOnePointXsAddon: function name() {
            let canUse = mainHelper.canUseCSharpAddon();
            if (canUse) {
                Frame.alert('敬请期待！');
            } else {
                Frame.alert('您需要安装Micosoft .Net Framwork 4.5或以上的运行环境才可以继续安装！');
            }
        },
        select: function () {
            this.$refs.controll.querySelector('#zipFile').click();
            // console.log(this.$refs.controll);

        },
        selected: function (e) {
            let packageFilePath = e.target.files[0].path;
            if (packageFilePath && packageFilePath.indexOf('.zip') > -1) {
                //如果工作目录没有插件目录，就创建一个
                if (!fs.existsSync(PLUGIN_DIR)) {
                    fs.mkdirSync(PLUGIN_DIR);
                }
                if (!fs.existsSync(PLUGIN_DIR + '/temp')) {
                    fs.mkdirSync(PLUGIN_DIR + '/temp');
                } else {
                    deleteall(PLUGIN_DIR + '/temp');
                }

                this.installProcess++;
                //解压插件包到临时目录中
                compressing.zip.uncompress(packageFilePath, PLUGIN_DIR + '/temp')
                    .then(() => {
                        //TODO 验证文件签名，验证通过的才可以移入插件目录，不通过的视为不安全的插件询问用户是否删除
                        let mkf = PLUGIN_DIR + '/temp/' + getUuid() + '.log';
                        fs.writeFileSync(mkf, packageFilePath + '\n');
                        app.installProcess++;
                        app.doInstall(mkf);
                    })

                    .catch(err => {
                        layer.msg("无法解压插件包(代号：ZI0003)！");
                        console.error(err);
                    });
            } else {
                layer.msg("没有选中正确的格式的文件！(代号：ZI0001)");
            }
        },
        doInstall: function (mkf) {
            this.installProcess++;
            if (fs.existsSync(PLUGIN_DIR + '/temp/info.json')) {
                fs.readFile(PLUGIN_DIR + '/temp/info.json', (err, data) => {
                    if (err) {
                        layer.msg("安装失败，不是标准的插件代码！(代号：ZI0004)");
                    }
                    try {
                        let json = JSON.parse(data);
                        fs.appendFileSync(mkf, json.title + '\n');
                        fs.appendFileSync(mkf, json.version + '\n');
                        if (!fs.existsSync(PLUGIN_DIR + '/' + json.title))
                        fs.mkdirSync(PLUGIN_DIR + '/' + json.title);
                        fs.copyFileSync(PLUGIN_DIR + '/temp/main.js', PLUGIN_DIR + '/' + json.title + '/main.js');
                        if (!fs.existsSync(PLUGIN_DIR + '/temp/main.html'))
                            fs.copyFileSync(PLUGIN_DIR + '/temp/main.html', PLUGIN_DIR + '/' + json.title + '/main.html');
                        let iconPath = null;
                        if (fs.existsSync(PLUGIN_DIR + '/temp/i.png')) {
                            fs.copyFileSync(PLUGIN_DIR + '/temp/i.png', PLUGIN_DIR + '/' + json.title + '/i.png');
                            iconPath = PLUGIN_DIR + '/' + json.title + '/i.png';
                        }
                        let res = api.getDB() ? api.getDB().run("insert into my_plugin (title,version,icon_path,scrpit) values (?,?,?,?)", [json.title, json.version, iconPath,'item.instance.admin();']) : 0;
                        if (res == 0) {
                            layer.msg("安装失败，请重启后再试！(代号：ZI0007)");
                        }
                        api.saveDB();
                        Frame.alert("安装完成！");
                    } catch (error) {
                        layer.msg("安装失败，不是标准的插件代码！(代号：ZI0005)");
                        fs.appendFileSync(mkf, error.message);
                    }
                    this.installProcess = 0;
                });
            } else {
                layer.msg("安装失败，不是标准的插件代码！(代号：ZI0006)");
                fs.appendFileSync(mkf, '没有info.json文件\n');
            }
        }
    }
});

/**
 * 公共方法
 */

/**
 * 遍历删除文件夹
 * @param {string} path 
 */
function deleteall(path) {
    let files = [];
    if (fs.existsSync(path)) {
        files = fs.readdirSync(path);
        files.forEach(function (file, index) {
            let curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) { // recurse
                deleteall(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}
/**
 * 获取UUID
 * @returns 
 */
function getUuid() {
    var len = 32;//32长度
    var radix = 16;//16进制
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    var uuid = [], i;
    radix = radix || chars.length;
    if (len) {
        for (i = 0; i < len; i++)uuid[i] = chars[0 | Math.random() * radix];
    } else {
        var r;
        uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
        uuid[14] = '4';
        for (i = 0; i < 36; i++) {
            if (!uuid[i]) {
                r = 0 | Math.random() * 16;
                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
            }
        }
    }
    return uuid.join('');
}