const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BabiliPlugin=require("babili-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'mod', 'mainWin', 'js', 'index.js'),
    tray: path.resolve(__dirname, 'mod', 'mainWin', 'js', 'tray.js'),
    sts: path.resolve(__dirname, 'mod', 'mainWin','js', 'sts.js'),
    vfps: path.resolve(__dirname, 'mod', 'mainWin','js', 'vfps.js'),
    vote: path.resolve(__dirname, 'mod', 'mainWin','js', 'vote.js'),
    localMusicLib: path.resolve(__dirname, 'mod', 'mainWin','js', 'localMusicLib.js'),
    OSD: path.resolve(__dirname, 'mod', 'OSD', 'js', 'index.js'),
    pluginInstall: path.resolve(__dirname, 'mod', 'pluginManager', 'js', 'install.js'),
    upgradePlanWin: path.resolve(__dirname, 'mod', 'upgradePlanWin', 'js', 'index.js'),
  },
  /**
   * 把NODE系统自带的模块在webpack打包器中忽略，下列模块会原样输出到前端页面(因为要NW.js要直接从页面执行这些命令)
   */
  externals:{
    fs: 'require("fs")',
    path: 'require("path")',
    child_process:'require("child_process")',
    http:'require("http")',
    zlib:'require("zlib")',
    stream:'require("stream")',
    nwGui:'require("nw.gui")',
    net:'require("net")',
    tls:'require("tls")',
    mainHelper:'require("main_helper.node")',
    dxHelper:'require("dx_helper.node")',
    url:'require("url")'
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'bw'),
    filename: '[name].bundle.js'
  },
  resolve: {
    fallback: { "buffer": false,"constants": false,"assert": false },
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 用 webpack 1 时需用 'vue/dist/vue.common.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [{
          loader: "style-loader"  // creates style nodes from JS strings
        }, {
          loader: "css-loader"    // translates CSS into CommonJS
        }, {
          loader: "less-loader"   // compiles Less to CSS
        }]
      },
      {
        test: require.resolve('jquery'),
        loader: 'expose-loader',
        options: {
          exposes: ['$', 'jQuery'],
        },
      },
      {
        test: require.resolve('vue'),
        loader: 'expose-loader',
        options: {
          exposes: ['mvvm'],
        },
      },
      // {
      //   test: require.resolve('sql-wasm'),
      //   loader: 'expose-loader',
      //   options: {
      //     exposes: ['sql'],
      //   },
      // },
      {
        test: /\.(png|jpg|gif|svg)$/, //用于css中调用被打包的图片
        loader: 'file-loader'
      },
      {
        test: /\.html$/,
        loader: 'html-withimg-loader'
      }
    ]
  },
  plugins: [
    // chunks的参数是跟上面的entry对象里面的属性一一对应的
    new HtmlWebpackPlugin({
      filename: "mainWindow.html",
      minify: false,
      template: path.resolve(__dirname, 'mod', 'mainWin', 'index.html'),
      chunks: ['main','tray']
    }),
    new HtmlWebpackPlugin({
      filename: "sts.html",
      template: path.resolve(__dirname, 'mod', 'mainWin', 'sts.html'),
      chunks: ['sts']
    }),
    new HtmlWebpackPlugin({
      filename: "vote.html",
      template: path.resolve(__dirname, 'mod', 'mainWin', 'vote.html'),
      chunks: ['vote']
    }),
    new HtmlWebpackPlugin({
      filename: "vfps.html",
      template: path.resolve(__dirname, 'mod', 'mainWin', 'vfps.html'),
      chunks: ['vfps']
    }),
    new HtmlWebpackPlugin({
      filename: "localMusicLib.html",
      template: path.resolve(__dirname, 'mod', 'mainWin', 'localMusicLib.html'),
      chunks: ['localMusicLib']
    }),
    new HtmlWebpackPlugin({
      filename: "OSD.html",
      template: path.resolve(__dirname, 'mod', 'OSD', 'index.html'),
      chunks: ['OSD']
    }),
    new HtmlWebpackPlugin({
      filename: "pluginInstall.html",
      template: path.resolve(__dirname, 'mod', 'pluginManager', 'install.html'),
      chunks: ['pluginInstall']
    }),
    new HtmlWebpackPlugin({
      filename: "upgradePlanWin.html",
      template: path.resolve(__dirname, 'mod', 'upgradePlanWin', 'index.html'),
      chunks: ['upgradePlanWin']
    }),
    new CopyPlugin(
      {
        patterns: [
          // This wasm file will be fetched dynamically when we initialize sql.js
          // It is important that we do not change its name, and that it is in the same folder as the js
          { from: 'node_modules/sql.js/dist/sql-wasm.js', to: './' },
          { from: 'node_modules/sql.js/dist/sql-wasm.wasm', to: './' },
          { from: 'mod/mainWin/blank.html', to: './' },
          { from: 'core/html/rdbw.html', to: './' },
          { from: 'node_modules/layer-src/dist/theme/', to: './theme/' }
        ],
      })
  ],
};